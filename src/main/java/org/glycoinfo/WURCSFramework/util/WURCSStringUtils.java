package org.glycoinfo.WURCSFramework.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for string in WURCS
 * @author Issaku Yamada
 * @author Masaaki Matsubara
 *
 */
public class WURCSStringUtils {

	/**
	 * Convert to URI string
	 * @param a_strUrl is a target String
	 * @return encoded String
	 */
	public static String getURLString(String a_strUrl){

		String t_strUrl = "";
		try {
			t_strUrl = URLEncoder.encode(a_strUrl,"utf-8");
			t_strUrl = t_strUrl.replaceAll("<", "%3C");
			t_strUrl = t_strUrl.replaceAll(">", "%3E");
			t_strUrl = t_strUrl.replaceAll("\\*", "%2A");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return t_strUrl;
	}

	/**
	 * Highlight a part of the given String.
	 * @param str String to be highlighted
	 * @param poss The position numbers of character in the given String.
	 * @return Highlighted String
	 */
	public static String highlight(String str, int... poss) {
		List<Integer> lPoss = new ArrayList<>();
		for ( int pos : poss )
			lPoss.add(pos);
		return highlight(str, lPoss);
	}

	/**
	 * Highlight a part of the given String.
	 * @param str String to be highlighted
	 * @param poss List of the position numbers of character in the given String.
	 * @return Highlighted String
	 */
	public static String highlight(String str, List<Integer> poss) {
		String strNew = "";
		for ( int i=0; i<str.length(); i++ ) {
			if ( poss.contains(i+1) && !poss.contains(i) )
				strNew += '"';
			strNew += str.charAt(i);
			if ( poss.contains(i+1) && !poss.contains(i+2) )
				strNew += '"';
		}
		return strNew;
	}

}
