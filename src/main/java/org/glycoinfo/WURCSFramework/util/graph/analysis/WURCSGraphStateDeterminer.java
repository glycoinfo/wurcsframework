package org.glycoinfo.WURCSFramework.util.graph.analysis;

import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.wurcs.graph.Backbone;
import org.glycoinfo.WURCSFramework.wurcs.graph.BackboneCarbon;
import org.glycoinfo.WURCSFramework.wurcs.graph.CarbonDescriptor;
import org.glycoinfo.WURCSFramework.wurcs.graph.Modification;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;

public class WURCSGraphStateDeterminer {

	public static boolean hasMultipleReducingEnd(WURCSGraph a_graph) throws WURCSException {
		return ( a_graph.getRootBackbones().size() > 1 );
	}

	public static boolean isNonReducingSugar(WURCSGraph a_graph) throws WURCSException {
		Modification modOnAnom = getAnomericModificationOnReducingEnd(a_graph);
		if ( modOnAnom == null )
			return false;
		return modOnAnom.isGlycosidic();
	}

	public static boolean isGlycoside(WURCSGraph a_graph) throws WURCSException {
		Modification modOnAnom = getAnomericModificationOnReducingEnd(a_graph);
		if ( modOnAnom == null )
			return false;
		return !modOnAnom.isGlycosidic();
	}

	public static Modification getAnomericModificationOnReducingEnd(WURCSGraph a_graph) throws WURCSException {
		if ( hasMultipleReducingEnd(a_graph) )
			return null;
		Backbone bbRoot = a_graph.getRootBackbones().get(0);
		return getAnomericModification(bbRoot);
	}

	public static boolean isGlycoside(Backbone a_bb) throws WURCSException {
		Modification modOnAnom = getAnomericModification(a_bb);
		if ( modOnAnom == null )
			return false;
		return !modOnAnom.isGlycosidic();
	}

	public static Modification getAnomericModification(Backbone a_bb) throws WURCSException {
		if ( a_bb.getAnomericEdge() == null )
			return null;
		return a_bb.getAnomericEdge().getModification();
	}

	public static boolean hasUncertainAnomericState(WURCSGraph a_graph) throws WURCSException {
		for ( Backbone bbRoot : a_graph.getRootBackbones() )
			if ( hasUncertainAnomericState(bbRoot) )
				return true;
		return false;
	}

	public static boolean hasUncertainAnomericState(Backbone a_bb) {
		for ( BackboneCarbon bc : a_bb.getBackboneCarbons() ) {
			if ( bc.getDescriptor() == CarbonDescriptor.SSX_UNDEF_U
			  || bc.getDescriptor() == CarbonDescriptor.SZX_UNDEF_L )
				return true;
		}
		return false;
	}

	public static boolean hasAnomerOnReducingEnd(WURCSGraph a_graph) throws WURCSException {
		for ( Backbone bbRoot : a_graph.getRootBackbones() )
			if ( hasAnomer(bbRoot) )
				return true;
		return false;
	}

	public static boolean hasAnomer(Backbone a_bb) {
		// TODO: Update for new anomer info management system
		return ( a_bb.getAnomericSymbol() != 'o' );
	}

	public static String getAnomericSymbolOnReducingEnd(WURCSGraph a_graph) throws WURCSException {
		// Only for standard WURCS, which has only one reducing end
		Backbone bbRoot = a_graph.getRootBackbones().get(0);
		return getAnomericSymbol(bbRoot);
	}

	public static String getAnomericSymbol(Backbone a_bb) {
		return ""+a_bb.getAnomericSymbol();
	}
}
