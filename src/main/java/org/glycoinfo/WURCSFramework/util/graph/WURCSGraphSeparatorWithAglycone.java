package org.glycoinfo.WURCSFramework.util.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.graph.visitor.WURCSVisitorCollectConnectingBackboneGroups;
import org.glycoinfo.WURCSFramework.wurcs.graph.Backbone;
import org.glycoinfo.WURCSFramework.wurcs.graph.Modification;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSEdge;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;

public class WURCSGraphSeparatorWithAglycone {

	public static class WURCSGraphForAglycone {
		private Map<Modification, List<WURCSEdge>> m_mapAglyconeToEdges;
		private Map<WURCSGraph, WURCSEdge> m_mapGraphToAglyconeEdge;

		private WURCSGraphForAglycone() {
			this.m_mapAglyconeToEdges = new HashMap<>();
			this.m_mapGraphToAglyconeEdge = new HashMap<>();
		}

		public boolean hasAglycone() {
			return !this.m_mapAglyconeToEdges.isEmpty();
		}

		public boolean isSeparated() {
			return !this.m_mapGraphToAglyconeEdge.isEmpty();
		}

		private void addAglycone(Modification a_modAglycone, List<WURCSEdge> a_edges) {
			this.m_mapAglyconeToEdges.put(a_modAglycone, a_edges);
		}

		public List<Modification> getAglycone() {
			return new ArrayList<>(this.m_mapAglyconeToEdges.keySet());
		}

		public List<WURCSEdge> getEdgesConnectingAglycone(Modification a_modAglycone) {
			return this.m_mapAglyconeToEdges.get(a_modAglycone);
		}

		public WURCSEdge getAglyconeEdgeConnectingWith(Backbone a_bb) {
			for ( Modification modA : this.m_mapAglyconeToEdges.keySet() ) {
				for ( WURCSEdge edge : this.m_mapAglyconeToEdges.get(modA) ) {
					if ( edge.getBackbone().equals(a_bb) )
						return edge;
				}
			}
			return null;
		}

		private boolean addSeparatedGraph(WURCSGraph a_graphSep) {
			// Add graph with null edge If no aglycone but graph is separated
			if ( this.m_mapAglyconeToEdges.isEmpty() ) {
				this.m_mapGraphToAglyconeEdge.put(a_graphSep, null);
				return true;
			}
			for ( Modification modA : this.m_mapAglyconeToEdges.keySet() ) {
				for ( WURCSEdge edge : this.m_mapAglyconeToEdges.get(modA) ) {
					for ( Backbone bb : a_graphSep.getBackbones() ) {
						if ( !edge.getBackbone().equals(bb) )
							continue;
						this.m_mapGraphToAglyconeEdge.put(a_graphSep, edge);
						return true;
					}
				}
			}
			return false;
		}

		public List<WURCSGraph> getSeparatedGraphs() {
			return new ArrayList<>( this.m_mapGraphToAglyconeEdge.keySet() );
		}

		public WURCSEdge getAglyconeEdgeFrom(WURCSGraph a_graphSep) {
			return this.m_mapGraphToAglyconeEdge.get(a_graphSep);
		}
	}

	public static WURCSGraphForAglycone start(WURCSGraph a_graph) throws WURCSException {
		WURCSGraphForAglycone graphA = new WURCSGraphForAglycone();

		WURCSGraph graphCopy = a_graph.copy();

		// Find modifications
		List<Modification> lModAs = getAglycones(graphCopy);
		if ( lModAs.isEmpty() )
			return graphA;

		// Remove aglycones from graph
		for ( Modification modA : lModAs ) {
			graphA.addAglycone(modA, new ArrayList<>(modA.getEdges()));
			graphCopy.removeModification(modA);
		}

		// Separate WURCSGraph with aglycone
		for ( WURCSGraph graphSep : separateGraph(graphCopy) )
			graphA.addSeparatedGraph(graphSep);

		return graphA;
	}

	private static List<Modification> getAglycones(WURCSGraph a_graph) {
		List<Modification> lModAs = new ArrayList<>();
		for ( Modification mod : a_graph.getModifications() ) {
			if ( !mod.isAglycone() )
				continue;
			if ( mod.canOmitMAP() )
				continue;
			lModAs.add(mod);
		}
		return lModAs;
	}

	/**
	 * Separate WURCSGraph to connected graphs
	 * @param a_graph WURCSGraph to be separated
	 * @return List of WURCSGraph which all nodes are connected
	 * @throws WURCSException
	 */
	public static List<WURCSGraph> separateGraph(WURCSGraph a_graph) throws WURCSException {
		WURCSVisitorCollectConnectingBackboneGroups t_oGroup = new WURCSVisitorCollectConnectingBackboneGroups();
		t_oGroup.start(a_graph);

		List<WURCSGraph> lSeparatedGraphs = new ArrayList<>();
		for ( Set<Backbone> lBGrp : t_oGroup.getBackboneGroups() ) {
			WURCSGraph graphNew = new WURCSGraph();
			for ( Backbone bb : lBGrp ) {
				List<WURCSEdge> lEdges = bb.getEdges();
				if ( lEdges.isEmpty() ) {
					graphNew.addBackbone(bb);
					continue;
				}
				for ( WURCSEdge edge : lEdges )
					graphNew.addResidues(bb, edge, edge.getModification());
			}
			lSeparatedGraphs.add(graphNew);
		}
		return lSeparatedGraphs;
	}
}
