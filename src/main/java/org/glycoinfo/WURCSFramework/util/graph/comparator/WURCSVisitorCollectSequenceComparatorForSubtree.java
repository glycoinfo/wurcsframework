package org.glycoinfo.WURCSFramework.util.graph.comparator;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;

import org.glycoinfo.WURCSFramework.util.graph.visitor.WURCSVisitorCollectSequence;
import org.glycoinfo.WURCSFramework.util.graph.visitor.WURCSVisitorException;
import org.glycoinfo.WURCSFramework.wurcs.graph.ModificationAlternative;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSEdge;

/**
 * Extension of WURCSVisitorCollectSequenceComparator for comparing WURCSVisitorCollectSequeces as subtrees.
 * @author Masaaki Matsubara
 *
 */
public class WURCSVisitorCollectSequenceComparatorForSubtree extends WURCSVisitorCollectSequenceComparator {

	private boolean m_bHasRelationship = true;

	private HashMap<WURCSVisitorCollectSequence, LinkedList<WURCSVisitorCollectSequence>> t_mapChildToParents = new HashMap<>();
	private HashMap<WURCSVisitorCollectSequence, LinkedList<WURCSVisitorCollectSequence>> t_mapParentToChildren = new HashMap<>();

	private HashMap<WURCSVisitorCollectSequence, Integer> t_mapParentSeqToHeight = new HashMap<>();
	private HashMap<WURCSVisitorCollectSequence, Integer> t_mapParentSeqToNumDescendant = new HashMap<>();

	/**
	 * Constructor. Each given WURCSVisitorCollectSequence must be separated each other but can be connected with ModificationAlternative.
	 * @param a_aSequences List of WURCSVisitorCollectSequences to be sorted
	 * @throws WURCSVisitorException
	 */
	public WURCSVisitorCollectSequenceComparatorForSubtree(LinkedList<WURCSVisitorCollectSequence> a_aSequences) throws WURCSVisitorException {
		// Check parent-child relationships for subtree
		for ( int i=0; i<a_aSequences.size()-1; i++ ) {
			WURCSVisitorCollectSequence t_oSeq1 = a_aSequences.get(i);
			for ( int j=i+1; j<a_aSequences.size(); j++ ) {
				WURCSVisitorCollectSequence t_oSeq2 = a_aSequences.get(j);

				int t_iComp = compareSubtreeRelationship(t_oSeq1, t_oSeq2);
				if ( t_iComp == 0 )
					continue;

				// Map parent and child
				WURCSVisitorCollectSequence t_oSeqParent = (t_iComp < 0)? t_oSeq1 : t_oSeq2;
				WURCSVisitorCollectSequence t_oSeqChild  = (t_iComp < 0)? t_oSeq2 : t_oSeq1;

//				System.out.println(t_oSeqParent.hashCode()+" <- "+t_oSeqChild.hashCode());

				if ( !t_mapChildToParents.containsKey(t_oSeqChild) )
					t_mapChildToParents.put(t_oSeqChild, new LinkedList<WURCSVisitorCollectSequence>());
				if ( !t_mapChildToParents.get(t_oSeqChild).contains(t_oSeqParent) )
					t_mapChildToParents.get(t_oSeqChild).add(t_oSeqParent);

				if ( !t_mapParentToChildren.containsKey(t_oSeqParent) )
					t_mapParentToChildren.put(t_oSeqParent, new LinkedList<WURCSVisitorCollectSequence>());
				if ( !t_mapParentToChildren.get(t_oSeqParent).contains(t_oSeqChild) )
					t_mapParentToChildren.get(t_oSeqParent).add(t_oSeqChild);
			}
		}

		// Return if no parent-child relationship
		if ( t_mapChildToParents.isEmpty() ) {
			this.m_bHasRelationship = false;
			return;
		}

		// Check subtree info of each sequence in glycan graph
		for ( WURCSVisitorCollectSequence t_oSeqParent : a_aSequences ) {
			// 0 for no children
			if ( !t_mapParentToChildren.containsKey(t_oSeqParent) ) {
				t_mapParentSeqToHeight.put(t_oSeqParent, 0);
				t_mapParentSeqToNumDescendant.put(t_oSeqParent, 0);
				continue;
			}
			LinkedList<WURCSVisitorCollectSequence> t_aSeqChildren = t_mapParentToChildren.get(t_oSeqParent);
			int t_iHeight = 0;
			int t_nDescendant = 0;
			while ( !t_aSeqChildren.isEmpty() ) {
				t_nDescendant += t_aSeqChildren.size();
				LinkedList<WURCSVisitorCollectSequence> t_aSeqChildrenNew = new LinkedList<>();
				for ( WURCSVisitorCollectSequence t_oSeqChild : t_aSeqChildren ) {
					if ( t_mapParentToChildren.containsKey(t_oSeqChild) )
						t_aSeqChildrenNew.addAll(t_mapParentToChildren.get(t_oSeqChild));
				}
				// Check cyclic relationship
				if ( t_aSeqChildrenNew.contains(t_oSeqParent) )
					throw new WURCSVisitorException("A cyclic subtree relationship is found.");
				t_aSeqChildren = t_aSeqChildrenNew;
				t_iHeight++;
			}
			t_mapParentSeqToHeight.put(t_oSeqParent, t_iHeight);
			t_mapParentSeqToNumDescendant.put(t_oSeqParent, t_nDescendant);
		}
	}

	/**
	 * Returns if the given sequences have subtree relationships
	 * @return {@code true} if there is relationship
	 */
	public boolean hasRelationship() {
		return this.m_bHasRelationship;
	}

	/**
	 * Compare parent-child relationship between two sequences.
	 * @param a_oSeq1
	 * @param a_oSeq2
	 * @return {@code -1} if a_oSeq2 is subtree of a_oSeq1, otherwise {@code 1}, or {@code 0} if no relationship between two sequences
	 */
	private static int compareSubtreeRelationship(WURCSVisitorCollectSequence a_oSeq1, WURCSVisitorCollectSequence a_oSeq2) {
		LinkedList<ModificationAlternative> t_lSubtrees1 = a_oSeq1.getSubtreeLinkageModifications();
		LinkedList<ModificationAlternative> t_lSubtrees2 = a_oSeq2.getSubtreeLinkageModifications();

		// Find shared modifications
		for ( ModificationAlternative t_oModAlt : t_lSubtrees1 ) {
			if ( !t_lSubtrees2.contains(t_oModAlt) )
				continue;

			// Check which is the parent sequence
			boolean t_bIsParent1 = false;
			boolean t_bIsParent2 = false;
			for ( WURCSEdge t_oEdge : t_oModAlt.getLeadInEdges() ) {
				if ( a_oSeq1.getNodes().contains(t_oEdge.getBackbone()) )
					t_bIsParent1 = true;
				if ( a_oSeq2.getNodes().contains(t_oEdge.getBackbone()) )
					t_bIsParent2 = true;
			}
			if (  t_bIsParent1 && !t_bIsParent2 ) return -1;
			if ( !t_bIsParent1 &&  t_bIsParent2 ) return  1;
//			if ( t_bIsParent1 && t_bIsParent2 )
//				System.out.println("Both parents: "+a_oSeq1.hashCode()+" vs "+a_oSeq2.hashCode());
		}
		return 0;
	}

	@Override
	public int compare(WURCSVisitorCollectSequence o1, WURCSVisitorCollectSequence o2) {
		int t_iComp = 0;

		if ( o1.equals(o2) ) return 0;

		// Compare parents
		t_iComp = m_compParents.compare(o1, o2);
		if ( t_iComp != 0 )
			return t_iComp;

		// Higher sequence comes first
		int t_iHeight1 = t_mapParentSeqToHeight.get(o1);
		int t_iHeight2 = t_mapParentSeqToHeight.get(o2);
		t_iComp = t_iHeight2 - t_iHeight1;
		if ( t_iComp != 0 )
			return t_iComp;

		// Sequence with many children comes first
		int t_nDescendant1 = t_mapParentSeqToNumDescendant.get(o1);
		int t_nDescendant2 = t_mapParentSeqToNumDescendant.get(o2);
		t_iComp = t_nDescendant2 - t_nDescendant1;
		if ( t_iComp != 0 )
			return t_iComp;

		return super.compare(o1, o2);
	}

	/**
	 * Private comparator only for comparing parents
	 */
	private Comparator<WURCSVisitorCollectSequence> m_compParents = new Comparator<WURCSVisitorCollectSequence>() {
		@Override
		public int compare(WURCSVisitorCollectSequence o1, WURCSVisitorCollectSequence o2) {
			if ( o1.equals(o2) ) return 0;

			// Prioritize no parent sequence
			boolean t_bHasParent1 = t_mapChildToParents.containsKey(o1);
			boolean t_bHasParent2 = t_mapChildToParents.containsKey(o2);
			if ( !t_bHasParent1 &&  t_bHasParent2 ) return -1;
			if (  t_bHasParent1 && !t_bHasParent2 ) return 1;
			if ( !t_bHasParent1 && !t_bHasParent2 ) return 0;

			// Compare parents
			return this.compareParents(t_mapChildToParents.get(o1), t_mapChildToParents.get(o2));
		}

		private int compareParents(LinkedList<WURCSVisitorCollectSequence> a_aParents1, LinkedList<WURCSVisitorCollectSequence> a_aParents2) {
			Collections.sort(a_aParents1, this);
			Collections.sort(a_aParents2, this);

			int t_nParents = a_aParents1.size();
			if ( t_nParents < a_aParents2.size() )
				t_nParents = a_aParents2.size();
			for ( int i=0; i<t_nParents; i++ ) {
				// Prioritize sequence with lesser number of parents
				if ( i >= a_aParents1.size() ) return -1;
				if ( i >= a_aParents2.size() ) return 1;

				WURCSVisitorCollectSequence t_oSeq1 = a_aParents1.get(i);
				WURCSVisitorCollectSequence t_oSeq2 = a_aParents2.get(i);
				int t_iComp = this.compare(t_oSeq1, t_oSeq2);
				if ( t_iComp != 0 )
					return t_iComp;
			}
			return 0;
		}
	};

}
