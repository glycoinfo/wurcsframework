package org.glycoinfo.WURCSFramework.util;

import java.util.LinkedList;

public class WURCSDataConverter {

	/**
	 * Convert RES ID number to RES Index string
	 * <table>
	 * <tr><th>ID</th><th>-></th><th>Index</th><tr>
	 * <tr><td>   1</td><td>-></td><td> a</td><tr>
	 * <tr><td>   2</td><td>-></td><td> b</td><tr>
	 * <tr><td>  26</td><td>-></td><td> z</td><tr>
	 * <tr><td>  27</td><td>-></td><td> A</td><tr>
	 * <tr><td>  28</td><td>-></td><td> B</td><tr>
	 * <tr><td>  52</td><td>-></td><td> Z</td><tr>
	 * <tr><td>  53</td><td>-></td><td>aa</td><tr>
	 * <tr><td>  54</td><td>-></td><td>ab</td><tr>
	 * <tr><td> 104</td><td>-></td><td>aZ</td><tr>
	 * <tr><td> 105</td><td>-></td><td>ba</td><tr>
	 * <tr><td> 106</td><td>-></td><td>bb</td><tr>
	 * <tr><td>1000</td><td>-></td><td>sl</td><tr>
	 * <tr><td>2000</td><td>-></td><td>Lx</td><tr>
	 * </table>
	 * @param a_iRESID
	 * @return String of RES Index
	 */
	public static String convertRESIDToIndex(int a_iRESID) {
		String t_strRes = "";

		int t_iRemainder = a_iRESID;
		LinkedList<Integer> t_aQuotients = new LinkedList<Integer>();
		while(t_iRemainder > 0) {
			int t_iQuotient = t_iRemainder % 52;
			t_iQuotient = (t_iQuotient == 0)? 52 : t_iQuotient;
			t_aQuotients.addFirst(t_iQuotient);
			t_iRemainder = (int)Math.floor( (t_iRemainder - t_iQuotient) / 52 );
		}

		// int of alphabet
		int t_iLower = (int)'a' - 1;
		int t_iUpper = (int)'A' - 1;
		for (Integer q : t_aQuotients) {
			int alphabet = ( q > 26 )? t_iUpper + q - 26 : t_iLower + q;
			t_strRes += (char)alphabet;
		}
		return t_strRes;
	}

	/**
	 * Convert RES Index string to RES ID number
	 * <table>
	 * <tr><th>Index</th><th>-></th><th>ID</th><tr>
	 * <tr><td> a</td><td>-></td><td>   1</td><tr>
	 * <tr><td> b</td><td>-></td><td>   2</td><tr>
	 * <tr><td> z</td><td>-></td><td>  26</td><tr>
	 * <tr><td> A</td><td>-></td><td>  27</td><tr>
	 * <tr><td> B</td><td>-></td><td>  28</td><tr>
	 * <tr><td> Z</td><td>-></td><td>  52</td><tr>
	 * <tr><td>aa</td><td>-></td><td>  53</td><tr>
	 * <tr><td>ab</td><td>-></td><td>  54</td><tr>
	 * <tr><td>aZ</td><td>-></td><td> 104</td><tr>
	 * <tr><td>ba</td><td>-></td><td> 105</td><tr>
	 * <tr><td>bb</td><td>-></td><td> 106</td><tr>
	 * <tr><td>sl</td><td>-></td><td>1000</td><tr>
	 * <tr><td>Lx</td><td>-></td><td>2000</td><tr>
	 * </table>
	 * @param a_iRESIndex
	 * @return RES ID number
	 */
	public static int convertRESIndexToID(String a_strRESIndex) {
		int t_iRESID = 0;
		for ( int i=0; i<a_strRESIndex.length(); i++ ) {
			char ch = a_strRESIndex.charAt(a_strRESIndex.length()-1-i);
			int q = (int)ch + 1;
			q += Character.isUpperCase(ch) ? 26 - (int)'A' : - (int)'a';
			t_iRESID += q * Math.pow(52, i);
		}
		return t_iRESID;
	}

	/**
	 * Convert LIP Direction character to number of priority order
	 * @param a_cDirection
	 * @return Number of priority order
	 */
	public static int convertDirectionToID(char a_cDirection) {
		return   a_cDirection == 'n' ? 0
			   : a_cDirection == 'u' ? 1
			   : a_cDirection == 'd' ? 2
			   : a_cDirection == 't' ? 3
			   : a_cDirection == 'e' ? 4
			   : a_cDirection == 'z' ? 5
			   : a_cDirection == 'x' ? 6 : 7;
	}

}
