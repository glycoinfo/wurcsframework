package org.glycoinfo.WURCSFramework.util.validation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WURCSValidationReport {

	protected static interface Type {
		public String getName();
	}

	private enum DefaultType implements Type {
		ERROR("Error"),
		WARNING("Warning"),
		UNVERIFIABLE("Unverifiable")
		;

		private String name;
		private DefaultType(String name) {
			this.name = name;
		}
		public String getName() {
			return this.name;
		}
	}

	private static class Report {
		private String strMessage;
		private String strTargetInfo;
		private Exception exception;
		private StackTraceElement steCalledAt;

		@Override
		public String toString() {
			String ret = String.format("[%s] %s",
					steCalledAt.getClassName(),
					strMessage
				);
			if ( strTargetInfo != null ) {
				ret = ret.replaceAll("\\.$", ""); // remove last period "."
				ret += ": "+strTargetInfo;
			}
			if ( exception != null )
				ret += String.format("\n\t[%s] %s", exception.getClass().getSimpleName(), exception.getMessage());
			return ret;
		}
	}

	private String m_sInputString;
	private String m_sStandardString;
	private List<String> m_lOutputStrings;

	private Map<Type, List<Report>> m_mapTypeToReports;

	public WURCSValidationReport() {
		this.m_mapTypeToReports = new HashMap<>();
		this.m_lOutputStrings = new ArrayList<>();
	}

	public String getInputString() {
		return m_sInputString;
	}

	public void setInputString(String a_sInputString) {
		this.m_sInputString = a_sInputString;
	}

	public String getStandardString() {
		return m_sStandardString;
	}

	public void setStandardString(String a_sStandardString) {
		this.m_sStandardString = a_sStandardString;
	}

	public void addOutputString(String a_sCandidateStandardString) {
		this.m_lOutputStrings.add(a_sCandidateStandardString);
	}

	public List<String> getOutputStrings() {
		return this.m_lOutputStrings;
	}

	public boolean isStandardized() {
		return !this.m_sInputString.equals(this.m_sStandardString);
	}

	protected void addReport(String a_strMessage, Type a_type, String a_strTargetInfo, Exception a_exception) {
		if ( !this.m_mapTypeToReports.containsKey(a_type) )
			this.m_mapTypeToReports.put(a_type, new ArrayList<>());

		Report repo = new Report();
		repo.strMessage = a_strMessage;
		repo.strTargetInfo = a_strTargetInfo;
		repo.exception = a_exception;
		repo.steCalledAt = new Throwable().getStackTrace()[2];

		this.m_mapTypeToReports.get(a_type).add(repo);
	}

	public void addError(String a_strMessage) {
		addReport(a_strMessage, DefaultType.ERROR, null, null);
	}

	public void addError(String a_strMessage, String a_strTargetInfo) {
		addReport(a_strMessage, DefaultType.ERROR, a_strTargetInfo, null);
	}

	public void addError(String a_strMessage, String a_strTargetInfo, Exception e) {
		addReport(a_strMessage, DefaultType.ERROR, a_strTargetInfo, e);
	}

	public void addWarning(String a_strMessage) {
		addReport(a_strMessage, DefaultType.WARNING, null, null);
	}

	public void addWarning(String a_strMessage, String a_strTargetInfo) {
		addReport(a_strMessage, DefaultType.WARNING, a_strTargetInfo, null);
	}

	public void addWarning(String a_strMessage, String a_strTargetInfo, Exception e) {
		addReport(a_strMessage, DefaultType.WARNING, a_strTargetInfo, e);
	}

	public void addUnverifiable(String a_strMessage) {
		addReport(a_strMessage, DefaultType.UNVERIFIABLE, null, null);
	}

	public void addUnverifiable(String a_strMessage, String a_strTargetInfo) {
		addReport(a_strMessage, DefaultType.UNVERIFIABLE, a_strTargetInfo, null);
	}

	public void addUnverifiable(String a_strMessage, String a_strTargetInfo, Exception e) {
		addReport(a_strMessage, DefaultType.UNVERIFIABLE, a_strTargetInfo, e);
	}

	protected boolean hasTypedReports(Type a_type) {
		if ( !this.m_mapTypeToReports.containsKey(a_type) )
			return false;
		return !this.m_mapTypeToReports.get(a_type).isEmpty();
	}

	public boolean hasError() {
		return hasTypedReports(DefaultType.ERROR);
	}

	public boolean hasWarning() {
		return hasTypedReports(DefaultType.WARNING);
	}

	public boolean hasUnverifiable() {
		return hasTypedReports(DefaultType.UNVERIFIABLE);
	}

	protected List<String> getReports(Type a_type) {
		List<String> t_lRepos = new ArrayList<>();
		if ( a_type == null || !this.m_mapTypeToReports.containsKey(a_type) )
			return t_lRepos;
		for ( Report repo : this.m_mapTypeToReports.get(a_type) ) {
			String strRepo = repo.strMessage;
			if ( repo.strTargetInfo != null ) {
				strRepo = strRepo.replaceAll("\\.$", ""); // remove last period "."
				strRepo += ": "+repo.strTargetInfo;
			}
			if ( repo.exception != null )
				strRepo += String.format(" - [%s] %s", repo.exception.getClass().getSimpleName(), repo.exception.getMessage());
			t_lRepos.add(strRepo);
		}
		return t_lRepos;
	}

	public List<String> getErrors() {
		return getReports(DefaultType.ERROR);
	}

	public List<String> getWarnings() {
		return getReports(DefaultType.WARNING);
	}

	public List<String> getUnverifiables() {
		return getReports(DefaultType.UNVERIFIABLE);
	}

	/**
	 * Returns the result contained in this report.
	 * <pre>
	 * {TYPE1}:
	 * {CLASS NAME} {MESSAGE}[ {TARGET INFO}][ {EXCEPTION INFO}]
	 * ...
	 * {TYPE2}:
	 * {CLASS NAME} {MESSAGE}[ {TARGET INFO}][ {EXCEPTION INFO}]
	 * ...
	 * </pre>
	 * @return
	 */
	public String getResults() {
		StringBuffer ret = new StringBuffer();
		for ( Type type : this.m_mapTypeToReports.keySet() ) {
			if ( this.m_mapTypeToReports.get(type).isEmpty() )
				continue;
			ret.append(type.getName());
			ret.append(":");
			ret.append(System.lineSeparator());
			for ( Report t_repoErr : this.m_mapTypeToReports.get(type) ) {
				ret.append(t_repoErr);
				ret.append(System.lineSeparator());
			}
		}

		if ( this.m_lOutputStrings.isEmpty() )
			return ret.toString();
		ret.append("Outputed strings:");
		ret.append(System.lineSeparator());
		for ( String t_strOut: this.m_lOutputStrings ) {
			ret.append(t_strOut);
			ret.append(System.lineSeparator());
		}

		if ( this.isStandardized() ) {
			ret.append("This WURCS is standardized.");
			ret.append(System.lineSeparator());
		}

		return ret.toString();
	}

	/**
	 * Returns the result contained in this report in simple format.
	 * <pre>
	 * {TYPE1}:
	 * {MESSAGE}
	 * ...
	 * {TYPE2}:
	 * {MESSAGE}
	 * ...
	 * </pre>
	 * @return
	 */
	public String getResultsSimple() {
		StringBuffer ret = new StringBuffer();
		for ( Type type : this.m_mapTypeToReports.keySet() ) {
			if ( this.m_mapTypeToReports.get(type).isEmpty() )
				continue;
			ret.append(type.getName());
			ret.append(":");
			ret.append(System.lineSeparator());
			Set<String> t_setMessages = new HashSet<>();
			for ( Report t_repo : this.m_mapTypeToReports.get(type) ) {
				if ( t_setMessages.contains(t_repo.strMessage) )
					continue;
				t_setMessages.add(t_repo.strMessage);
				ret.append(t_repo.strMessage);
				ret.append(System.lineSeparator());
			}
		}

		if ( this.isStandardized() ) {
			ret.append("This WURCS is standardized.");
			ret.append(System.lineSeparator());
		}

		return ret.toString();
	}
}
