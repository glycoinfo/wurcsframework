package org.glycoinfo.WURCSFramework.util.validation;

import java.util.HashMap;
import java.util.Map;

import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.util.array.WURCSExporter;
import org.glycoinfo.WURCSFramework.wurcs.array.LIN;
import org.glycoinfo.WURCSFramework.wurcs.array.MOD;
import org.glycoinfo.WURCSFramework.wurcs.array.RES;
import org.glycoinfo.WURCSFramework.wurcs.array.UniqueRES;
import org.glycoinfo.WURCSFramework.wurcs.array.WURCSArray;
import org.glycoinfo.WURCSFramework.wurcs.graph.Backbone;
import org.glycoinfo.WURCSFramework.wurcs.graph.Modification;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;

public class WURCSFactoryForValidation extends WURCSFactory {

	public static class BackboneInfo {
		private int m_iURESID;
		private String m_strRESIndex;
		private String m_strMS;

		public BackboneInfo(int a_iURESID, String a_strRESIndex, String a_strMS ) {
			this.m_iURESID = a_iURESID;
			this.m_strRESIndex = a_strRESIndex;
			this.m_strMS = a_strMS;
		}

		public int getURESID() {
			return this.m_iURESID;
		}

		public String getRESIndex() {
			return this.m_strRESIndex;
		}

		public String getMSString() {
			return this.m_strMS;
		}
	}

	public static class ModificationInfo {
		private String m_strMOD;

		public ModificationInfo(String a_strMOD) {
			this.m_strMOD = a_strMOD;
		}

		public String getString() {
			return this.m_strMOD;
		}
	}

	private Map<Backbone, BackboneInfo> m_mapBackboneToInfo;
	private Map<Modification, ModificationInfo> m_mapModificationToInfo;

	public WURCSFactoryForValidation(String a_strWURCS) throws WURCSException {
		super(a_strWURCS);
		mapInfo();
	}

	public WURCSFactoryForValidation(WURCSArray a_oArray) throws WURCSException {
		super(a_oArray);
		mapInfo();
	}

	public WURCSFactoryForValidation(WURCSGraph a_oGraph) throws WURCSException {
		super(a_oGraph);
		mapInfo();
	}

	public WURCSFactoryForValidation(String a_strWURCS, boolean a_bDoNormalize) throws WURCSException {
		super(a_strWURCS, a_bDoNormalize);
		mapInfo();
	}

	public WURCSFactoryForValidation(WURCSArray a_oArray, boolean a_bDoNormalize) throws WURCSException {
		super(a_oArray, a_bDoNormalize);
		mapInfo();
	}

	public WURCSFactoryForValidation(WURCSGraph a_oGraph, boolean a_bDoNormalize) throws WURCSException {
		super(a_oGraph, a_bDoNormalize);
		mapInfo();
	}

	public BackboneInfo getInfo(Backbone a_oBackbone) {
		return this.m_mapBackboneToInfo.get(a_oBackbone);
	}

	public ModificationInfo getInfo(Modification a_oModification) {
		return this.m_mapModificationToInfo.get(a_oModification);
	}

	private void mapInfo() {
		WURCSExporter t_oExport = new WURCSExporter();

		this.m_mapBackboneToInfo = new HashMap<>();
		for ( Backbone t_bb : this.getGraph().getBackbones() ) {
			UniqueRES ures = this.m_oG2A.getUniqueRES(t_bb);
			RES res = this.m_oG2A.getRES(t_bb);

			this.m_mapBackboneToInfo.put(t_bb,
					new BackboneInfo(res.getUniqueRESID(), res.getRESIndex(), t_oExport.getUniqueRESString(ures)));
		}

		this.m_mapModificationToInfo = new HashMap<>();
		for ( Modification t_mod : this.getGraph().getModifications() ) {
			MOD mod = this.m_oG2A.getMOD(t_mod);
			if ( mod != null ) {
				this.m_mapModificationToInfo.put(t_mod, new ModificationInfo(t_oExport.getMODString(mod)));
				continue;
			}
			LIN lin = this.m_oG2A.getLIN(t_mod);
			if ( lin != null ) {
				this.m_mapModificationToInfo.put(t_mod, new ModificationInfo(t_oExport.getLINString(lin)));
				continue;
			}
		}
	}

}
