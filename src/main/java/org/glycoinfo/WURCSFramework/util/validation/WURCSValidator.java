package org.glycoinfo.WURCSFramework.util.validation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.util.WURCSStringUtils;
import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;
import org.glycoinfo.WURCSFramework.util.graph.visitor.WURCSVisitorException;
import org.glycoinfo.WURCSFramework.util.map.MAPFactory;
import org.glycoinfo.WURCSFramework.util.property.AtomicProperties;
import org.glycoinfo.WURCSFramework.wurcs.graph.Backbone;
import org.glycoinfo.WURCSFramework.wurcs.graph.BackboneCarbon;
import org.glycoinfo.WURCSFramework.wurcs.graph.CarbonDescriptor;
import org.glycoinfo.WURCSFramework.wurcs.graph.DirectionDescriptor;
import org.glycoinfo.WURCSFramework.wurcs.graph.InterfaceRepeat;
import org.glycoinfo.WURCSFramework.wurcs.graph.LinkagePosition;
import org.glycoinfo.WURCSFramework.wurcs.graph.Modification;
import org.glycoinfo.WURCSFramework.wurcs.graph.ModificationAlternative;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSEdge;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPAtom;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPAtomAbstract;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPAtomCyclic;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPBondType;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPConnection;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPGraph;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPStar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for validating WURCS and finding errors and warnings.
 * 
 * @author Masaaki Matsubara
 */
public class WURCSValidator {

	private static final Logger logger = LoggerFactory.getLogger(WURCSValidator.class);

	private WURCSValidationReport m_repo;

	private WURCSFactoryForValidation m_factory;
	private Map<String, MAPFactory> m_mapMAPStringToFactory;

	private int m_nMaxBranches;

	public WURCSValidator() {
		this.m_repo = getNewReport();
		this.m_mapMAPStringToFactory = new HashMap<>();

		this.m_nMaxBranches = 10;
	}

	/**
	 * Set limitation of branch count on a MAP (default is 10).
	 * @param a_nBranches The number of branches to be unverifiable
	 */
	public void setMaxBranchCount(int a_nBranches) {
		this.m_nMaxBranches = a_nBranches;
	}

	public WURCSValidationReport getReport() {
		return this.m_repo;
	}

	protected WURCSFactoryForValidation getWURCSFactory() {
		return this.m_factory;
	}

	protected WURCSValidationReport getNewReport() {
		return new WURCSValidationReport();
	}

	/**
	 * Starts validation.
	 * 
	 * @param a_strWURCS String of WURCS to be validated
	 */
	public void start(String a_strWURCS) {
		this.getReport().setInputString(a_strWURCS);
		try {
			this.m_factory = new WURCSFactoryForValidation(a_strWURCS);

			WURCSGraph graph = this.m_factory.getGraph();

			// Validate
			this.validate(graph);

			// Do not output WURCS if error or unverifiable
			if ( this.getReport().hasError()
			  || this.getReport().hasUnverifiable() )
				return;

			// normalize MAP before output
			this.m_factory.normalizeMAP();

			// Check output WURCS
			String t_strOutWURCS = this.m_factory.getWURCS();
			// Normalize recursively
			while (true) {
				this.getReport().addOutputString(t_strOutWURCS);

				WURCSFactory factoryNew = new WURCSFactory(t_strOutWURCS);
				String t_strOutWURCSNew = factoryNew.getWURCS();
				// Stop recursion if same as previous one
				if ( t_strOutWURCS.equals(t_strOutWURCSNew) ) {
					if ( this.getReport().getOutputStrings().size() > 1 )
						this.getReport().addWarning("The normalization process was performed recursively.");
					break;
				}

				t_strOutWURCS = t_strOutWURCSNew;

				if ( !this.getReport().getOutputStrings().contains(t_strOutWURCS) )
					continue;

				// Stop recursion if circulated normalization
				this.getReport().addWarning("The normalization process is circulated."
						+ " The most prior one in the results is selected as standard.");
				// Sort outputs as String and choose first one as standard
				List<String> t_lOutWURCSs = new ArrayList<>(this.getReport().getOutputStrings());
				Collections.sort(t_lOutWURCSs);
				t_strOutWURCS = t_lOutWURCSs.get(0);
				break;
			}

			this.getReport().setStandardString(t_strOutWURCS);

		} catch (WURCSException e) {
			logger.error("Error in parsing WURCS.", e);
			this.getReport().addError("Error in parsing WURCS due to the exception.", a_strWURCS, e);
		} catch (Exception e) {
			logger.error("Something wrong with the WURCS.", a_strWURCS, e);
			this.getReport().addError("Something wrong with the WURCS.", a_strWURCS, e);
		}
	}

	private void validate(WURCSGraph a_graph) {
		// Validate WURCSGraph
		try {
			this.validateGraph(a_graph);
		} catch (WURCSVisitorException e) {
			logger.error("Error in analyzing WURCSGraph.", e);
			this.getReport().addError("Error in analyzing WURCSGraph due to the exception.",
					this.m_factory.getWURCS(), e);
		}
		// Validate Backbones
		for (Backbone t_bb : a_graph.getBackbones())
			this.validateBackbone(t_bb);
		// Validate Modifications
		for (Modification t_mod : a_graph.getModifications())
			this.validateModification(t_mod);
	}

	/**
	 * Validate the WURCSGraph.
	 * @param a_graph WURCSGraph to be validated
	 * @throws WURCSVisitorException
	 */
	protected void validateGraph(WURCSGraph a_graph) throws WURCSVisitorException {
		// Should be validated using graph analyzer
		// TODO: Check composition
	}

	/**
	 * Validate the Backbone. This validates Anomers,
	 * connections on CarbonDescriptors, and connections between carbons.
	 * @param a_bb Backbone to be validated
	 */
	protected void validateBackbone(Backbone a_bb) {
		this.validateBackboneCarbons(a_bb);
		this.validateAnomer(a_bb);
		this.validateConnectionsOnCarbonDescriptors(a_bb);
		this.validateCarbonConnections(a_bb);
	}

	/**
	 * Validate the Modification. This validates MAPs and repeating units.
	 * 
	 * @param a_mod Modification to be validated
	 */
	protected void validateModification(Modification a_mod) {
		validateMAP(a_mod);
		// TODO: validate repeat and alternative
		validateRepeat(a_mod);
	}

	protected MAPFactory getMAPFactory(String a_strMAP) {
		if ( this.m_mapMAPStringToFactory.containsKey(a_strMAP) )
			return this.m_mapMAPStringToFactory.get(a_strMAP);

		MAPFactory t_factory = null;
		try {
			// Create MAPFactory without normalization
			t_factory = new MAPFactory(a_strMAP);
		} catch (WURCSFormatException e) {
			logger.error("Error in parsing MAP string: {}", a_strMAP, e);
			this.getReport().addError("Error in parsing MAP string.", a_strMAP, e);
		}
		this.m_mapMAPStringToFactory.put(a_strMAP, t_factory);
		return t_factory;
	}

	/**
	 * Validate carbons of backbone.  If there is error or something wrong, the
	 * messages are stored.
	 * 
	 * @param a_bb Backbone to be checked its carbons
	 */
	private void validateBackboneCarbons(Backbone a_bb) {
		if ( a_bb.getBackboneCarbons().isEmpty() )
			this.getReport().addError("A Backbone has no carbon.");
	}

	/**
	 * Validates anomer state of backbone. If there is error or something wrong, the
	 * messages are stored.
	 * 
	 * @param a_bb Backbone to be checked anomer state
	 */
	private void validateAnomer(Backbone a_bb) {

		int apos = a_bb.getAnomericPosition();
		char asymb = a_bb.getAnomericSymbol();

		String strMS = this.m_factory.getInfo(a_bb).getMSString();
		int posApos = strMS.indexOf('-')+2;
		int posAsymb = posApos+1;

		// Format check
		if (asymb != 'a' && asymb != 'b' && asymb != 'u' && asymb != 'd' && asymb != 'x' && asymb != 'o')
			this.getReport().addError("Invalid anomeric symbol is used, which must be 'a', 'b', 'u', 'd', 'x' and 'o'.",
					WURCSStringUtils.highlight(strMS, posAsymb));

		// Inconsistency between position and symbol
		if ( asymb == 'o' && apos != 0 )
			this.getReport().addError("Anomeric position must not be specified when anomeric symbol 'o' is specified.",
					WURCSStringUtils.highlight(strMS, posApos));

		// Ring check
		if ( apos > 0 ) {
			boolean hasRing = false;
			for ( WURCSEdge edge0 : a_bb.getEdges() ) {
				if ( edge0.getLinkages().size() != 1 )
					continue;
				int pos = edge0.getLinkages().get(0).getBackbonePosition();
				if ( pos != apos )
					continue;
				Modification mod = edge0.getModification();
				if ( mod.isGlycosidic() )
					continue;
				if ( mod.getEdges().size() != 2 )
					continue;
				hasRing = true;
			}
			if ( !hasRing )
				this.getReport().addError("There is no ring making anomer despite existing anomeric information.",
						WURCSStringUtils.highlight(strMS, posApos));
		}

		if ( a_bb.hasUnknownLength() ) {
			return;
		}

		// Check inconsistency for SkeletonCode and anomeric info
		List<Integer> t_lPossU = new ArrayList<>();
		List<Integer> t_lPossO = new ArrayList<>();
		List<Integer> t_lPossAnom = new ArrayList<>();

		for (int i = 0; i < a_bb.getBackboneCarbons().size(); i++) {
			BackboneCarbon t_bc = a_bb.getBackboneCarbons().get(i);
			int pos = i + 1;

			if (t_bc.getDescriptor().getChar() == 'u' || t_bc.getDescriptor().getChar() == 'U')
				t_lPossU.add(pos);

			if (t_bc.getDescriptor().getChar() == 'o' || t_bc.getDescriptor().getChar() == 'O')
				t_lPossO.add(pos);

			if (t_bc.getDescriptor().getChar() == 'a')
				t_lPossAnom.add(pos);
		}

		logger.debug("Checking (potential) anomeric CarbonDescriptors:");
		logger.debug("Target SkeletonCode: {}", a_bb.getSkeletonCode());
		logger.debug("Unknown position(s): {}", t_lPossU);
		logger.debug("Calbonyl position(s): {}", t_lPossO);
		logger.debug("Anomeric position(s): {}", t_lPossAnom);

		// Inconsistency for 'u' and 'a'
		if ( !t_lPossU.isEmpty() && !t_lPossAnom.isEmpty() ) {
			List<Integer> lPoss = new ArrayList<>();
			lPoss.addAll(t_lPossU);
			lPoss.addAll(t_lPossAnom);
			this.getReport().addError("CarbonDescriptors 'a' and 'u' must not exist at the same time in the same SkeletonCode.",
					WURCSStringUtils.highlight(strMS,lPoss));
		}

		// No anomeric info
		if ( !t_lPossAnom.isEmpty() ) {
			if ( t_lPossAnom.size() > 1 )
				// Inconsistency for # of 'a'
				this.getReport().addError("CarbonDescritptor 'a' must be only one per SkeletonCode.",
						WURCSStringUtils.highlight(strMS,t_lPossAnom));
			if ( t_lPossAnom.get(0) != apos ) {
				List<Integer> t_lPoss = new ArrayList<>();
				t_lPoss.addAll(t_lPossAnom);
				t_lPoss.add(posApos);
				this.getReport().addError("Anomeric position and position of CarbonDescriptor 'a' in SkeletonCode must be matched.",
						WURCSStringUtils.highlight(strMS,t_lPoss));
			}
			if ( apos < 1 && asymb == 'o' )
				this.getReport().addError("Anomeric information must be specified when SkeletonCode has CarbonDescriptor 'a'.",
						WURCSStringUtils.highlight(strMS,t_lPossAnom));
		} else if (apos != 0 || asymb != 'o')
			// Anomeric position and symbol must be 0 and 'o', respectively
			this.getReport().addError("SkeletonCode with no CarbonDescriptor 'a' must not have anomeric information.",
					WURCSStringUtils.highlight(strMS, posApos, posAsymb));

		// TODO: check relative anomeric symbol is specified but no anomeric reference atom
	}

	/**
	 * Validates the number of linkages and modification types connecting to backbone carbons
	 * @param a_bb Backbone to be validated
	 */
	private void validateConnectionsOnCarbonDescriptors(Backbone a_bb) {
		// No check for the backbone with unknown length
		if ( a_bb.hasUnknownLength() )
			return;

		// Classify modifications with the linkage positions
		Map<Integer, List<WURCSEdge>> t_mapPosToEdges = new HashMap<>();
		List<WURCSEdge> t_lAltEdges = new ArrayList<>();
		List<WURCSEdge> t_lRepEdges = new ArrayList<>();
		for (WURCSEdge t_edge : a_bb.getEdges()) {
			int t_iPos = -1;
			if (t_edge.getLinkages().size() == 1)
				t_iPos = t_edge.getLinkages().getFirst().getBackbonePosition();
			if ( t_iPos != -1 && t_iPos > a_bb.getBackboneCarbons().size() )
				this.getReport().addError("The connected modification has wrong linkage position for the Backbone.",
						t_iPos+" on "+a_bb.getSkeletonCode());

			Modification t_mod = t_edge.getModification();
			if ( t_mod instanceof InterfaceRepeat ) {
				t_lRepEdges.add(t_edge);
				continue;
			}
			if ( t_mod instanceof ModificationAlternative ) {
				ModificationAlternative t_modAlt = (ModificationAlternative)t_mod;
				if ( t_modAlt.getLeadInEdges().contains(t_edge)
					|| t_modAlt.getLeadOutEdges().contains(t_edge)	)
					t_lAltEdges.add(t_edge);
			}

			if ( !t_mapPosToEdges.containsKey(t_iPos) )
				t_mapPosToEdges.put(t_iPos, new ArrayList<>());
			t_mapPosToEdges.get(t_iPos).add(t_edge);
		}

		// TODO: Add repeating edge if there is a normal edge with the same position
/*
		for ( WURCSEdge edgeRep : t_lRepEdges ) {
			
		}
*/

		boolean t_bHasUnknownCarbon = false;
		int t_nMinConnsTotal = 0;
		int t_nMaxConnsTotal = 0;
		for (BackboneCarbon t_bc : a_bb.getBackboneCarbons()) {
			int t_iPos = a_bb.getBackboneCarbons().indexOf(t_bc) + 1;
			char t_cCD = t_bc.getDescriptor().getChar();

			String t_strTargetInfo = WURCSStringUtils.highlight(this.m_factory.getInfo(a_bb).getMSString(), t_iPos);
			if ( t_cCD == '?' ) {
				this.getReport().addWarning("Unknown CarbonDescriptor is contained.", t_strTargetInfo);
				t_bHasUnknownCarbon = true;
				continue;
			}

			// Check number of unique without hydrogens
			int t_nMods = t_bc.getDescriptor().getNumberOfModifications();
			t_nMods -= t_bc.getDescriptor().getNumberOfHydrogens();

			// Check the connecting modifications are different
			int t_nUniqueMods = t_bc.getDescriptor().getNumberOfUniqueModifications();
			t_nUniqueMods -= (t_bc.getDescriptor().getNumberOfHydrogens() > 0)? 1 : 0;
			if ( t_nUniqueMods < 0 )
				t_nUniqueMods = 0;

			int t_nMinConns = (t_nUniqueMods == 0)? 0 : t_nUniqueMods - 1;
			if ( t_cCD == 'A' ) // for modification "*=O"
				t_nMinConns -= 1;
			int t_nMaxConns = t_nMods;
			if ( t_cCD == 'Q' ) {
				t_nMinConns = (t_bc.getDescriptor().isTerminal())? 1 : 0;
				t_nMaxConns = t_nMinConns+2;
			}

			// Count max and min number of modifications on the backbone
			t_nMinConnsTotal += t_nMinConns;
			t_nMaxConnsTotal += t_nMaxConns;

			// Get modifications connected to the carbon
			List<WURCSEdge> t_lModEdges = t_mapPosToEdges.get(t_iPos);
			if ( t_lModEdges == null )
				t_lModEdges = new ArrayList<>();

			// Count alternative edges
			int t_nAlt = 0;
			for ( WURCSEdge edge : t_lModEdges )
				if ( t_lAltEdges.contains(edge) )
					t_nAlt++;

			// Check inconsistency for number of linkages
			String t_strMessage = null;
			if ( t_lModEdges.size() > t_nMaxConns ) {
				if ( t_nMaxConns == 0 )
					t_strMessage = "No modification is allowed to be connected to the CarbonDescriptor \""+t_cCD+"\".";
				else if ( t_nMaxConns == 1 )
					t_strMessage = "Only one modification is allowed to be connected to the CarbonDescriptor \""+t_cCD+"\".";
				else
					t_strMessage = "Too many modifications are connected to the CarbonDescriptor \""+t_cCD+"\".";
			} else if ( t_lModEdges.size() < t_nMinConns ) {
				t_strMessage = "At least "+t_nMods+" modifications (containing an omitted hydroxyl group) must be connected to the CarbonDescriptor \""+t_cCD+"\".";
			}
			if ( t_strMessage != null ) {
				if ( t_nAlt > 0 )
					this.getReport().addWarning(t_strMessage, t_strTargetInfo);
				else
					this.getReport().addError(t_strMessage, t_strTargetInfo);
				continue;
			}

			// No check for 'Q'
			if ( t_cCD == 'Q' )
				continue;

			// Check linkage directions
			int t_nNoDirection = 0;
			int t_nDirection = 0;
			Set<DirectionDescriptor> t_setDDs = new HashSet<>();
			for ( WURCSEdge t_edge : t_lModEdges ) {
				DirectionDescriptor dd = t_edge.getLinkages().get(0).getDirection();
				if ( dd == DirectionDescriptor.N || dd == DirectionDescriptor.L ) {
					t_nNoDirection++;
				} else {
					t_nDirection++;
					t_setDDs.add(dd);
					if ( t_bc.getDescriptor().getStereo() != null || t_bc.getDescriptor() == CarbonDescriptor.DZ2_CISTRANS_XU )
						this.getReport().addWarning("DirectionDescriptor must not be specified for the CarbonDescriptor \""+t_cCD+"\".",
								t_strTargetInfo);
				}

				if ( dd.getOrbital() != null && !dd.getOrbital().equals(t_bc.getDescriptor().getHybridOrbital()))
					this.getReport().addError("The DirectionDescriptor \""+dd.getName()+"\" can not be used for the CarbonDescriptor \""+t_cCD+"\".",
							t_strTargetInfo);
			}
			if ( t_nDirection != t_setDDs.size() ) {
				if ( !t_setDDs.contains(DirectionDescriptor.X) )
					this.getReport().addError("Every DirectionDescriptor except for 'n' on the same linkage position must be different."
							,t_strTargetInfo);
				else if ( t_setDDs.size() > 2 )
					this.getReport().addError("The DirectionDescriptor \"x\" must not be specified with the other DirectionDescriptor on the same linkage position."
							,t_strTargetInfo);
			}
			if ( t_nMods != t_nUniqueMods && t_nNoDirection >= t_nUniqueMods )
				this.getReport().addError("The linkages on the CarbonDescriptor must have direction.", t_strTargetInfo);

			// Check available connections
			Map<String, Integer> t_mapModToCount = new HashMap<>();
			for ( int i=1; i<=3; i++ ) {
				String t_strMod = t_bc.getDescriptor().getModification(i);
				if ( t_strMod == null )
					continue;
				if ( !t_mapModToCount.containsKey(t_strMod) )
					t_mapModToCount.put(t_strMod, 0);
				int t_nMod = t_mapModToCount.get(t_strMod)+1;
				t_mapModToCount.put(t_strMod, t_nMod);
			}

			// Compare two or more modifications
//			if ( t_lModEdges.size() < 2 )
//				continue;

			// Check inconsistency for number of unique modifications
			Set<String> t_setMODStrings = new HashSet<>();
			// Collect connecting atoms
			for ( WURCSEdge t_edge : t_lModEdges ) {
				if ( t_edge.getModification().canOmitMAP() ) {
					t_setMODStrings.add("");
					continue;
				}
				int t_iModPos = t_edge.getLinkages().get(0).getModificationPosition();
				String t_strMAP = t_edge.getModification().getMAPCode();
				MAPFactory factory = this.getMAPFactory(t_strMAP);
				if ( factory == null )
					continue;
				MAPGraph graph = factory.getMAPGraph();
				MAPConnection conn = null;
				for ( MAPStar star : graph.getStars() ) {
					if ( t_iModPos != star.getStarIndex() )
						continue;
					conn = star.getConnection();
					break;
				}
				String t_strTargetMAP = this.m_factory.getInfo(t_edge.getModification()).getString();
				if ( conn == null ) {
//					this.getReport().addWarning("The corresponding StarIndex in the linkage can not be found from the MAP.", t_strTargetMAP);
					continue;
				}
				if ( conn.getBondType() == MAPBondType.AROMATIC || conn.getBondType() == MAPBondType.UNKNOWN ) {
					this.getReport().addWarning("The bond type from the MAP star \"*\" must not be aromatic or unknown.", t_strTargetMAP);
					continue;
				}
				String t_strConn = ""+conn.getBondType().getSymbol();
				if ( !t_mapModToCount.containsKey(t_strConn+"X") )
					this.getReport().addError("The modification can not be connected to the CarbonDescriptor \""+t_cCD+"\".",
							t_strTargetMAP);
				t_strConn += conn.getAtom().getSymbol();
				if ( t_strConn.equals("-O") || t_strConn.equals("=O") )
					t_strConn = "";
				t_setMODStrings.add(t_strConn);
			}

			if ( t_setMODStrings.size() == t_nUniqueMods || t_setMODStrings.size() == t_nUniqueMods-1 )
				continue;
			// Two unique mods "*O" and "*=O" can be omitted only for the CarbonDescriptor "A"
			if ( t_bc.getDescriptor() == CarbonDescriptor.SZ2_ACID_U
				&& t_setMODStrings.size() == t_nUniqueMods-2 )
				continue;
			this.getReport().addError("The number of unique modifications connected to the CarbonDescriptor \""+t_cCD+"\" must be "+t_nUniqueMods
					+" (containing omitted hydroxyl groups), but "+t_setMODStrings.size()+" unique modifications are connected.",
					t_strTargetInfo);

		}

		// Check total number of modifications for the backbone if containing unknown position linkages
		if ( !t_mapPosToEdges.containsKey(-1) || t_bHasUnknownCarbon )
			return;

		// TODO: check alternative edges for all residues
		String t_strTargetInfo = String.format("RESIndex \"%s\" (%s)",
				this.m_factory.getInfo(a_bb).getRESIndex(),
				this.m_factory.getInfo(a_bb).getMSString());
		if ( a_bb.getEdges().size() > t_nMaxConnsTotal ) {
			if ( t_lAltEdges.isEmpty() )
				this.getReport().addError("Too many linkages on the backbone.", t_strTargetInfo);
			else
				this.getReport().addWarning("Too many linkages on the backbone.", t_strTargetInfo);
		} else if ( a_bb.getEdges().size() < t_nMinConnsTotal ) {
			this.getReport().addError("Too few linkages on the backbone.", t_strTargetInfo);
		}
	}

	/**
	 * Validates connection between backbone carbons
	 * @param a_bb Backbone to be validated
	 */
	private void validateCarbonConnections(Backbone a_bb) {
		// No check for backbone with one backbone carbon (unknown backbone)
		if ( a_bb.getBackboneCarbons().size() < 2 )
			return;

		int t_iBond2Pre = 0;
		CarbonDescriptor t_cdPre = null;
		boolean t_bHasMismatch = false;
		int t_nUnknown = 0;
		boolean t_bWasUnknown = false;
		for (BackboneCarbon t_bc : a_bb.getBackboneCarbons()) {
			int t_iPos = a_bb.getBackboneCarbons().indexOf(t_bc) + 1;
			CarbonDescriptor t_cd = (CarbonDescriptor)t_bc.getDescriptor();

			t_bHasMismatch = false;

			int t_iPosT2 = t_iPos + t_nUnknown*2;
			int t_iPosT1 = t_iPosT2 - 1 - ((t_bWasUnknown)? 1 : 0);
			if ( t_bc.hasUnknownLength() ) {
				t_nUnknown++;
				t_iPosT2 += 1;
				t_bWasUnknown = true;
			} else
				t_bWasUnknown = false;
			String t_strTargetInfo = WURCSStringUtils.highlight(this.m_factory.getInfo(a_bb).getMSString(), t_iPosT1, t_iPosT2);
			if ( t_cdPre == null && t_cd.isTerminal() != null && !t_cd.isTerminal() ) {
				this.getReport().addError("The CarbonDescriptor \""+t_cd+"\" must not be at terminal.",
						t_strTargetInfo);
				t_bHasMismatch = true;
			}

			// Check bond order between two backbone carbons
			int t_iBond1 = t_cd.getBondTypeCarbon1();
			int t_iBond2 = t_cd.getBondTypeCarbon2();

			if ( t_iBond2Pre < 0 || t_iBond1 < 0 || t_iBond2 < 0 ) {
				// TODO: check unknown carbon connection, e.g. "Q" or "?"
				this.getReport().addWarning("Can not check backbone carbon connections for unknown carbons.",
						t_strTargetInfo);
				t_bHasMismatch = true;
			} else {
				// Swap Bond1 and 2 at once if bond orders are not match
				if ( t_iBond1 != t_iBond2Pre ) {
					t_iBond2 = t_cd.getBondTypeCarbon1();
					t_iBond1 = t_cd.getBondTypeCarbon2();
				}
				// Inconsistency if not matching bond orders
				if (t_iBond1 != t_iBond2Pre) {
					this.getReport().addError("Bond order between two connecting backbone carbons is not matched.",
							t_strTargetInfo);
					t_bHasMismatch = true;
				}
			}

			t_iBond2Pre = t_iBond2;

			if ( t_cdPre == null ) {
				// Set no stereo double bond if terminal ketene
				if ( t_cd == CarbonDescriptor.DZ1_KETENE_U ) // "K" (terminal)
					t_cd = CarbonDescriptor.DZ2_METHYLENE_U; // "N" (terminal)
				t_cdPre = t_cd;
				continue;
			}

			// Skip check bond stereo if bond orders are mismatched
			if ( t_bHasMismatch ) {
				t_cdPre = t_cd;
				continue;
			}

			// Stereo check for double bond
			if ( t_iBond1 == 2 ) {
				// Skip checking for ketene to compare previous descriptor
				if ( t_cd == CarbonDescriptor.DD1_KETENE ) // "K" (non-terminal)
					continue;

				// Compare stereos between double bond carbons
				String t_strStereoPre = t_cdPre.getStereo();
				if ( t_strStereoPre == null )
					t_strStereoPre = "N";
				String t_strStereo = t_cd.getStereo();
				if ( t_strStereo == null )
					t_strStereo = "N";
				if ( !t_strStereoPre.equals(t_strStereo) ) {
					this.getReport().addError("Two carbons on a double bond must have the same stereochemistry.",
							t_strTargetInfo);
				}
			}

			t_cdPre = t_cd;
		}

		// Check terminal carbon
		if ( !t_bHasMismatch && t_iBond2Pre != 0 ) {
			this.getReport().addError("The CarbonDescirptor \""+t_cdPre+"\" must not be at terminal.",
					WURCSStringUtils.highlight(this.m_factory.getInfo(a_bb).getMSString(), a_bb.getBackboneCarbons().size()));
		}
	}

	private void validateMAP(Modification a_mod) {
		String t_strTargetMod = this.m_factory.getInfo(a_mod).getString();

		// Check number of linkages on the modification
		int t_nEdges = a_mod.getEdges().size();
		if ( a_mod instanceof ModificationAlternative ) {
			ModificationAlternative modAlt = (ModificationAlternative)a_mod;
			t_nEdges = 0;
			if ( !modAlt.getLeadInEdges().isEmpty() )
				t_nEdges++;
			if ( !modAlt.getLeadOutEdges().isEmpty() )
				t_nEdges++;
			for ( WURCSEdge t_edge : a_mod.getEdges() ) {
				if ( modAlt.getLeadInEdges().contains(t_edge) )
					continue;
				if ( modAlt.getLeadOutEdges().contains(t_edge) )
					continue;
				t_nEdges++;
			}
		}
		logger.debug("# of edges on modification: {}", t_nEdges);

		if ( t_nEdges == 1 && a_mod.getMAPCode().isEmpty() )
			this.getReport().addError("The modification with single linkage must have MAP, which can not be omitted.",
					t_strTargetMod);

		if (a_mod.canOmitMAP()) {
			if (t_nEdges > 3)
				this.getReport().addError("The modification with omitted MAP must not have three or more linkages.",
						t_strTargetMod);
			return;
		}

		// Check StarIndices on the MAP
		String t_strMAP = a_mod.getMAPCode();
		logger.debug("Check MAP {}", t_strMAP);
		MAPFactory t_factory = this.getMAPFactory(t_strMAP);
		if ( t_factory == null )
			return;

		// Check MAP update
		if (!t_factory.getMAPString().equals(t_strMAP))
			this.getReport().addWarning("The MAP has been updated.", t_strMAP + " -> " + t_factory.getMAPString());

		t_strMAP = t_factory.getMAPString();

		MAPGraph t_graph = t_factory.getMAPGraph();

		// Check total branch count
		int t_nBranchTotal = 0;
		for ( MAPAtomAbstract t_atom : t_graph.getAtoms() ) {
			if ( t_atom instanceof MAPAtomCyclic )
				continue;
			int nConn = t_atom.getConnections().size();
			t_nBranchTotal += Math.max(nConn - 2, 0);
		}
		// Stop MAP validation and mark as unverifiable if branch count is exceeded limitation
		if ( t_nBranchTotal > this.m_nMaxBranches ) {
			this.getReport().addUnverifiable(
					"Too many branches on the MAP.",
					String.format("%d/%d: %s", t_nBranchTotal, this.m_nMaxBranches, t_strMAP)
				);
			return;
		}

		// Update MAPFactory with normalization
		t_factory = new MAPFactory(t_graph);
		t_factory.normalize();

		// Check MAP change
		boolean t_bMAPChange = !t_factory.getMAPString().equals(t_strMAP);
		if ( t_bMAPChange )
			this.getReport().addWarning("The MAP could be normalized.", t_strMAP+" -> "+t_factory.getMAPString());


		// Check unknown atom in MAP
		for ( MAPAtomAbstract t_atomAbs : t_graph.getAtoms() ) {
			if ( !(t_atomAbs instanceof MAPAtom) )
				continue;
			MAPAtom t_atom =(MAPAtom)t_atomAbs;
			if ( AtomicProperties.forSymbol(t_atom.getSymbol()) == null )
				this.getReport().addWarning("Undefined atom symbol is used.", t_atom.getSymbol()+" in "+t_strMAP);
		}

		// Collect aromatic atoms for each part
		Set<MAPAtomAbstract> t_setCheckedAtom = new HashSet<>();
		List<List<MAPAtomAbstract>> t_lAromaticAtomGroups = new ArrayList<>();
		for ( MAPAtomAbstract t_atom : t_graph.getAtoms() ) {
			if ( !t_atom.isAromatic() )
				continue;
			if ( t_setCheckedAtom.contains(t_atom) )
				continue;

			// Collect aromatic atoms connecting aromatic atoms
			List<MAPAtomAbstract> t_lAromaticAtomGroup = new ArrayList<>();
			LinkedList<MAPAtomAbstract> t_lParentAromaticAtoms = new LinkedList<>();
			t_lParentAromaticAtoms.add(t_atom);
			while ( !t_lParentAromaticAtoms.isEmpty() ) {
				MAPAtomAbstract t_atomParent = t_lParentAromaticAtoms.removeFirst();
				t_lAromaticAtomGroup.add(t_atomParent);
				for ( MAPConnection t_conn : t_atomParent.getChildConnections() )
					if ( t_conn.getAtom().isAromatic()
					  && !t_lAromaticAtomGroup.contains(t_conn.getAtom()) )
						t_lParentAromaticAtoms.add(t_conn.getAtom());
			}
			t_lAromaticAtomGroups.add(t_lAromaticAtomGroup);
			t_setCheckedAtom.addAll(t_lAromaticAtomGroup);
		}
		// Check ring closure for each aromatic atoms
		for ( List<MAPAtomAbstract> t_lAromaticAtomGroup : t_lAromaticAtomGroups ) {
			int t_nBranch = 0;
			int t_nCyclic = 0;
			for ( MAPAtomAbstract t_atom : t_lAromaticAtomGroup ) {
				int t_nAromaticChildren = 0;
				for ( MAPConnection t_conn : t_atom.getChildConnections() )
					if ( t_conn.getAtom().isAromatic() )
						t_nAromaticChildren++;
				if ( t_nAromaticChildren > 1 )
					t_nBranch += t_nAromaticChildren - 1;
				if ( t_atom instanceof MAPAtomCyclic )
					t_nCyclic++;
			}
			if ( t_nBranch + 1 == t_nCyclic )
				continue;
			this.getReport().addError("This MAP has an aromatic group with wrong ring closure count"
					+ " (# of \"$\" is incorrect).",
					t_strTargetMod);
			break;
		}

		// Check stereo in aromatic
		if ( this.findStereoInAromatic(t_graph) )
			this.getReport().addWarning("Aromatic ring have stereochemistry which should be removed.", t_strTargetMod);


		// Check star indices
		List<Integer> t_lStarIndices = new ArrayList<>();
		for ( MAPStar t_star : t_graph.getStars() ) {
			int t_index = t_star.getStarIndex();
			logger.debug("has StarIndex {}", t_index );
			t_lStarIndices.add(t_index);
		}

		if ( t_lStarIndices.isEmpty() ) {
			this.getReport().addError("The MAP must have at least one star \"*\".", t_strMAP);
			return;
		}

		if ( t_nEdges != t_lStarIndices.size() )
			this.getReport().addError("The number of linkages connecting to the MAP must be equal to the number of stars \"*\" on the MAP.",
					t_strTargetMod);

		// Check if the MAP connects Backbone carbons directly
		boolean hasConnection = false;
		for ( MAPStar t_star : t_graph.getStars() )
			for ( MAPConnection t_conn : t_star.getConnections() )
				if ( t_graph.getStars().contains(t_conn.getAtom()) )
					hasConnection = true;
		if ( hasConnection )
			this.getReport().addError("The MAPStar \"*\" must not connect to the other MAPStar directly.", t_strTargetMod);


		// Can not check the matching between StarIndex on MAP and ModificaitonPosition on linkage if MAP has been normalized
		if ( t_bMAPChange ) {
			this.getReport().addWarning("The validation for the linkages on the MAP can not be checked, because the StarIndices on the MAP may also be changed.");
			return;
		}

		Set<Integer> t_setUniqueStars = new HashSet<Integer>(t_lStarIndices);
		if ( t_setUniqueStars.contains(0) && t_setUniqueStars.size() > 1 )
			this.getReport().addError("\"0\" must not be specified as StarIndex.", t_strMAP);

		for ( WURCSEdge t_edge : a_mod.getEdges() ) {
			for ( LinkagePosition t_link : t_edge.getLinkages() ) {
				int t_modPos = t_link.getModificationPosition();
				logger.debug("Check StarIndex {} on linkage", t_modPos);
				if ( t_setUniqueStars.contains( t_modPos ) )
					continue;
				if ( t_modPos != 0 )
					this.getReport().addError("The linkage must not have the StarIndex when the MAP has no StarIndex.",
							t_strTargetMod );
				else
					this.getReport().addError("The linkage must have one of the StarIndices in the MAP.",
							t_strTargetMod);
			}
		}

		// TODO: Check through MAPGraph

	}

	private boolean findStereoInAromatic(MAPGraph a_graph) {
		for ( MAPAtomAbstract t_atom : a_graph.getAtoms() ) {
			if ( !t_atom.isAromatic() )
				continue;
			if ( t_atom.getStereo() != null )
				return true;
			for ( MAPConnection t_conn : t_atom.getChildConnections() )
				if ( t_conn.getAtom().isAromatic() && t_conn.getStereo() != null )
					return true;
		}
		return false;
	}

	private void validateRepeat(Modification a_mod) {
		if ( !(a_mod instanceof InterfaceRepeat) )
			return;

		InterfaceRepeat t_rep = (InterfaceRepeat)a_mod;
		if ( t_rep.getMaxRepeatCount() > 0 && t_rep.getMaxRepeatCount() < t_rep.getMinRepeatCount() ) {
			this.getReport().addError("The max repeat count must be larger than min repeat count.", this.m_factory.getInfo(a_mod).getString());
		}
	}

}
