package org.glycoinfo.WURCSFramework.util.map;

import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;
import org.glycoinfo.WURCSFramework.util.map.analysis.MAPGraphNormalizer;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPGraph;

public class MAPFactory {

	private String m_strMAP;
	private MAPGraph m_oMAPGraph;

	public MAPFactory(String a_strMAP) throws WURCSFormatException {
		this(MAPGraphImporter.parseMAP(a_strMAP));
	}

	public MAPFactory(MAPGraph a_oMAPGraph) {
		this.m_oMAPGraph = a_oMAPGraph;
		this.m_strMAP = MAPGraphExporter.getMAP(this.m_oMAPGraph);
	}

	/**
	 * Normalize MAPGraph. Note that this method changes input MAP string and MAPGraph.
	 */
	public void normalize() {
		MAPGraphNormalizer t_oNormGraph = new MAPGraphNormalizer(this.m_oMAPGraph);
		t_oNormGraph.start();
		this.m_oMAPGraph = t_oNormGraph.getNormalizedGraph();
		this.m_strMAP = MAPGraphExporter.getMAP(this.m_oMAPGraph);
	}

	public String getMAPString() {
		return this.m_strMAP;
	}

	public MAPGraph getMAPGraph() {
		return this.m_oMAPGraph;
	}
}
