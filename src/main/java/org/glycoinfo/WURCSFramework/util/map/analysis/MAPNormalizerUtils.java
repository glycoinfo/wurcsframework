package org.glycoinfo.WURCSFramework.util.map.analysis;

import java.util.List;

import org.glycoinfo.WURCSFramework.util.map.analysis.MAPGraphExtractor.AromaticGroup;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPAtom;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPAtomAbstract;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPConnection;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPGraph;

public class MAPNormalizerUtils {

	/**
	 * Remove stereo information on aromatic atoms and connections
	 * @param a_oGraph
	 */
	public static void removeStereoOnAromatic( MAPGraph a_oGraph ) {
		List<AromaticGroup> lAromaticGroups = MAPGraphExtractor.extractAromaticGroups(a_oGraph);
		for ( AromaticGroup t_oAromaticGroup : lAromaticGroups ) {
			for ( MAPAtomAbstract t_oAtom : t_oAromaticGroup.getAromaticAtoms() )
				if ( t_oAtom instanceof MAPAtom )
					((MAPAtom)t_oAtom).setStereo(null);
			for ( MAPConnection t_oConn : t_oAromaticGroup.getAromaticConnections() )
				t_oConn.setStereo(null);
		}
	}

}
