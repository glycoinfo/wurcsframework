package org.glycoinfo.WURCSFramework.util.map;

import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;
import org.glycoinfo.WURCSFramework.util.map.analysis.MAPNormalizerUtils;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPGraph;

public class MAPNormalizer {

	private String m_strMAPInput;
	private String m_strMAPOutput;

	public MAPNormalizer(String a_strMAP) {
		this.m_strMAPInput = a_strMAP;
		this.m_strMAPOutput = a_strMAP;
	}

	public String getInput() {
		return this.m_strMAPInput;
	}

	public String getOutput() {
		return this.m_strMAPOutput;
	}

	public void start() throws WURCSFormatException {
		MAPGraph t_oGraph = MAPGraphImporter.parseMAP(this.m_strMAPInput);

		MAPNormalizerUtils.removeStereoOnAromatic(t_oGraph);

		this.m_strMAPOutput = MAPGraphExporter.getMAP(t_oGraph);
	}

}
