package org.glycoinfo.WURCSFramework.util.map.analysis;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.glycoinfo.WURCSFramework.wurcs.map.MAPAtomAbstract;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPConnection;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPGraph;

public class MAPGraphExtractor {

	public static class AromaticGroup {
		private List<MAPAtomAbstract> m_aAromaticAtoms;
		private List<MAPConnection> m_aAromaticConns;

		public List<MAPAtomAbstract> getAromaticAtoms() {
			return this.m_aAromaticAtoms;
		}

		public List<MAPConnection> getAromaticConnections() {
			return this.m_aAromaticConns;
		}
	}

	private MAPGraphExtractor() {};

	public static List<AromaticGroup> extractAromaticGroups(MAPGraph a_graph) {
		Set<MAPAtomAbstract> t_setCheckedAtom = new HashSet<>();
		List<AromaticGroup> t_lAromaticGroups = new ArrayList<>();
		for ( MAPAtomAbstract t_atom : a_graph.getAtoms() ) {
			if ( !t_atom.isAromatic() )
				continue;
			if ( t_setCheckedAtom.contains(t_atom) )
				continue;

			// Collect aromatic atoms connecting aromatic atoms
			List<MAPAtomAbstract> t_lAromaticAtomGroup = new ArrayList<>();
			List<MAPConnection> t_lAromaticConns = new ArrayList<>();
			LinkedList<MAPAtomAbstract> t_lParentAromaticAtoms = new LinkedList<>();
			t_lParentAromaticAtoms.add(t_atom);
			while ( !t_lParentAromaticAtoms.isEmpty() ) {
				MAPAtomAbstract t_atomParent = t_lParentAromaticAtoms.removeFirst();
				t_lAromaticAtomGroup.add(t_atomParent);
				for ( MAPConnection t_conn : t_atomParent.getChildConnections() )
					if ( t_conn.getAtom().isAromatic()
					  && !t_lAromaticAtomGroup.contains(t_conn.getAtom()) ) {
						t_lParentAromaticAtoms.add(t_conn.getAtom());
						t_lAromaticConns.add(t_conn);
						t_lAromaticConns.add(t_conn.getReverse());
					}
			}
			// Create aromatic group
			AromaticGroup t_lAromaticGroup = new AromaticGroup();
			t_lAromaticGroup.m_aAromaticAtoms = t_lAromaticAtomGroup;
			t_lAromaticGroup.m_aAromaticConns = t_lAromaticConns;
			t_lAromaticGroups.add(t_lAromaticGroup);

			t_setCheckedAtom.addAll(t_lAromaticAtomGroup);
		}

		return t_lAromaticGroups;
	}
}
