package org.glycoinfo.WURCSFramework.util;

import org.glycoinfo.WURCSFramework.util.array.WURCSImporter;
import org.glycoinfo.WURCSFramework.util.exchange.WURCSArrayToGraph;
import org.glycoinfo.WURCSFramework.util.graph.analysis.WURCSGraphStateDeterminer;
import org.glycoinfo.WURCSFramework.wurcs.array.MS;
import org.glycoinfo.WURCSFramework.wurcs.graph.Backbone;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;

public class AnomerCheckUtils {

	/**
	 * Returns {@code true} if the given composition WURCS is valid for glycosylation.
	 * @param a_strWURCSComposition String of WURCS for composition
	 * @return {@code true} if the given composition WURCS is valid for glycosylation,
	 * {@code false} otherwise.
	 */
	public static boolean isValidCompositionForGlycosylation(String a_strWURCSComposition) {
		try {
			WURCSGraph graph = toWURCSGraph(a_strWURCSComposition);

			// Do not allow composition containing monosaccharide with uncertain anomeric state
			if ( WURCSGraphStateDeterminer.hasUncertainAnomericState(graph) )
				return false;

			// Do not allow if no residue has anomeric symbol in the composition
			if ( !WURCSGraphStateDeterminer.hasAnomerOnReducingEnd(graph) )
				return false;

		} catch (WURCSException e) {
			// TODO: Handle error
			return false;
		}
		return true;
	}

	/**
	 * Returns anomeric symbol on the reducing end of the given WURCS.
	 * @param a_strWURCS String of WURCS
	 * @return String of anomeric symbol on the reducing end of the given WURCS or
	 * {@code null} for following cases: no anomeric info on the reducing end,
	 * no or multiple reducing end is 
	 */
	public static String getAnomericSymbol(String a_strWURCS) {
		try {
			WURCSGraph graph = toWURCSGraph(a_strWURCS);

			// Can not handle WURCS with multiple reducing ends
			if ( WURCSGraphStateDeterminer.hasMultipleReducingEnd(graph) )
				return null;

			// Can not handle WURCS with uncertain anomeric state
			if ( WURCSGraphStateDeterminer.hasUncertainAnomericState(graph) )
				return null;

			// Can not handle WURCS with no anomer on reducing end
			if ( !WURCSGraphStateDeterminer.hasAnomerOnReducingEnd(graph) )
				return null;

			// Do not check anomer for non-reducing sugar and glycoside
			if ( WURCSGraphStateDeterminer.getAnomericModificationOnReducingEnd(graph) != null )
				return null;

			return WURCSGraphStateDeterminer.getAnomericSymbolOnReducingEnd(graph);
		} catch (WURCSException e) {
			// TODO: Handle error
		}

		return null;
	}

	/**
	 * Returns anomeric symbol of the given ResidueCode.
	 * @param a_strResidueCode
	 * @return String of anomeric symbol of the given ResidueCode or
	 * {@code null} if no anomeric info.
	 */
	public static String getAnomericSymbolForResidueCode(String a_strResidueCode) {
		try {
			Backbone bb = toBackbone(a_strResidueCode);

			// Can not handle ResidueCode with uncertain anomeric state
			if ( WURCSGraphStateDeterminer.hasUncertainAnomericState(bb) )
				return null;

			if ( !WURCSGraphStateDeterminer.hasAnomer(bb) )
				return null;

			// Do not check anomer for glycoside
			if ( WURCSGraphStateDeterminer.isGlycoside(bb) )
				return null;

			return WURCSGraphStateDeterminer.getAnomericSymbol(bb);
		} catch (WURCSException e) {
			// TODO: Handle error
		}
		return null;
	}

	private static WURCSGraph toWURCSGraph(String a_strWURCS) throws WURCSException {
		WURCSFactory factory = new WURCSFactory(a_strWURCS);
		WURCSGraph graph = factory.getGraph();

		return graph;
	}

	private static Backbone toBackbone(String a_strResidueCode) throws WURCSException {
		MS ms = (new WURCSImporter()).extractMS(a_strResidueCode);

		WURCSArrayToGraph a2g = new WURCSArrayToGraph();
		Backbone bb = a2g.convertToBackbone(ms);

		return bb;
	}
}
