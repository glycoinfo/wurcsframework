package org.glycoinfo.WURCSFramework.wurcs.graph;

public enum AnomericSymbol {

	NONE   ('o', 0, false), // no anomer (open chain)
	UP     ('u', 1, true), // up,   absolute representation
	DOWN   ('d', 2, true), // down, absolute representation
	ALPHA  ('a', 3, true), // alpha anomer
	BETA   ('b', 4, true), // beta anomer
	UNKNOWN('x', 5, true), // anomer uknown
	NOFORM ('u', 6, false); // form unknown (open chain or ring)

	private char m_cName;
	private int  m_iScore;
	private boolean m_bHasConfiguration;

	private AnomericSymbol(char a_cSymbol, int a_iScore, boolean a_bHasCofiguration) {
		this.m_cName = a_cSymbol;
		this.m_iScore = a_iScore;
		this.m_bHasConfiguration = a_bHasCofiguration;
	}

	public char getName() {
		return this.m_cName;
	}

	public int getScore() {
		return this.m_iScore;
	}

	public boolean hasAnomericConfiguration() {
		return this.m_bHasConfiguration;
	}

	public static AnomericSymbol forChar(char a_cSymbol) {
		for ( AnomericSymbol t_enumAnom : AnomericSymbol.values() ) {
			if ( t_enumAnom.m_cName == a_cSymbol ) return t_enumAnom;
		}
		return null;
	}

}
