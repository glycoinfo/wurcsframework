package org.glycoinfo.WURCSFramework.wurcs.graph;


/**
 * Class for carbon descriptor of backbone SkeletonCode
 * @author MasaakiMatsubara
 *
 */
public enum CarbonDescriptor implements ICarbonDescriptor {

	// Terminal
	SZ3_METHYL_L   ( 'm', SP3, 1, 0, 1, null, false,    H,    H,    H,  4 ), // -C(H)(H)(H)
	SZ3_METHYL_U   ( 'M', SP3, 1, 0, 1, null, false,   X1,   X1,   X1, 10 ), // -C(X)(X)(X)
	SZ3_HYDROXYL   ( 'h', SP3, 1, 0, 2, null, false,    H,    H,   X1,  6 ), // -C(H)(H)(X)
	SZ3_ACETAL_L   ( 'c', SP3, 1, 0, 2, null, false,   X1,   X1,    H,  8 ), // -C(X)(X)(H)
	SZ3_ACETAL_U   ( 'C', SP3, 1, 0, 2, null, false,   X1,   X1,   X1, 10 ), // -C(X)(X)(X)
	SZ3_STEREO_S_L ( '1', SP3, 1, 0, 3,    S, false,   X1,   X1,    H, 10 ), // -C(X)(Y)(H)
	SZ3_STEREO_R_L ( '2', SP3, 1, 0, 3,    R, false,   X1,   X1,    H,  9 ), // -C(X)(Y)(H)
	SZ3_STEREO_s_L ( '3', SP3, 1, 0, 3,    s, false,   X1,   X1,    H, 10 ), // -C(X)(Y)(H)
	SZ3_STEREO_r_L ( '4', SP3, 1, 0, 3,    r, false,   X1,   X1,    H,  9 ), // -C(X)(Y)(H)
	SZ3_STEREO_X_L ( 'x', SP3, 1, 0, 3,    X, false,   X1,   X1,    H,  8 ), // -C(X)(Y)(H)
	SZ3_STEREO_S_U ( '5', SP3, 1, 0, 3,    S, false,   X1,   X1,   X1, 12 ), // -C(X)(Y)(Z)
	SZ3_STEREO_R_U ( '6', SP3, 1, 0, 3,    R, false,   X1,   X1,   X1, 11 ), // -C(X)(Y)(Z)
	SZ3_STEREO_s_U ( '7', SP3, 1, 0, 3,    s, false,   X1,   X1,   X1, 12 ), // -C(X)(Y)(Z)
	SZ3_STEREO_r_U ( '8', SP3, 1, 0, 3,    r, false,   X1,   X1,   X1, 11 ), // -C(X)(Y)(Z)
	SZ3_STEREO_X_U ( 'X', SP3, 1, 0, 3,    X, false,   X1,   X1,   X1, 10 ), // -C(X)(Y)(Z)
	SZ2_ALDEHYDE   ( 'o', SP2, 1, 0, 2, null, false,   X2,    H, null, 17 ), // -C(=X)(H)
	SZ2_ACID_U     ( 'A', SP2, 1, 0, 2, null, false,   X2,   X1, null, 14 ), // -C(=X)(Y)
	DZ2_METHYLENE_L( 'n', SP2, 2, 0, 1, null, false,    H,    H, null,  4 ), // =C(H)(H)
	DZ2_METHYLENE_U( 'N', SP2, 2, 0, 1, null, false,   X1,   X1, null,  8 ), // =C(X)(X)
	DZ2_CISTRANS_EL( 'e', SP2, 2, 0, 2,    E, false,   X1,    H, null,  8 ), // =C(X)(H)
	DZ2_CISTRANS_ZL( 'z', SP2, 2, 0, 2,    Z, false,   X1,    H, null,  7 ), // =C(X)(H)
	DZ2_CISTRANS_XL( 'f', SP2, 2, 0, 2,    X, false,   X1,    H, null,  7 ), // =C(X)(H)
	DZ2_CISTRANS_EU( 'E', SP2, 2, 0, 2,    E, false,   X1,   X1, null, 10 ), // =C(X)(Y)
	DZ2_CISTRANS_ZU( 'Z', SP2, 2, 0, 2,    Z, false,   X1,   X1, null,  9 ), // =C(X)(Y)
	DZ2_CISTRANS_XU( 'F', SP2, 2, 0, 2,    X, false,   X1,   X1, null,  8 ), // =C(X)(Y)
	SZ1_XETHYNE    ( 'T', SP , 1, 0, 1, null, false,   X3, null, null, 10 ), // -C(#X)
	DZ1_KETENE_U   ( 'K', SP , 2, 0, 1, null, false,   X2, null, null,  6 ), // =C(=X)
	TZ1_ETHYNE_L   ( 't', SP , 3, 0, 1, null, false,    H, null, null,  4 ), // #C(H)
	TZ1_ETHYNE_U   ( 'T', SP , 3, 0, 1, null, false,   X1, null, null,  6 ), // #C(X)
	// Anomeric
	SZX_ANOMER     ( 'a', SP3, 1, 0, 3, null,  true,   X1,   X1,    H, 17 ), // -C(X)(Y)(H) at anomeric position
	// Ambiguous
	SZX_UNDEF_L    ( 'u', SPX, 1, 0,-1, null,  null,    H, null, null, 17 ), // -C(X)(Y)(H) or -C(=O)(H)
	SZX_UNDEF_ALL  ( 'Q', SPX,-1, 0,-1, null,  null, null, null, null,  0 ), // terminal carbon type undefined at all
	// Non-terminal
	SS3_METHYNE    ( 'd', SP3, 1, 1, 1, null, false,    H,    H, null,  4 ), // -C(H)(H)-
	SS3_ACETAL     ( 'C', SP3, 1, 1, 1, null, false,   X1,   X1, null,  8 ), // -C(X)(X)-
	SS3_STEREO_S_L ( '1', SP3, 1, 1, 2,    S, false,   X1,    H, null,  8 ), // -C(X)(Y)-
	SS3_STEREO_R_L ( '2', SP3, 1, 1, 2,    R, false,   X1,    H, null,  7 ), // -C(X)(Y)-
	SS3_STEREO_s_L ( '3', SP3, 1, 1, 2,    s, false,   X1,    H, null,  8 ), // -C(X)(Y)-
	SS3_STEREO_r_L ( '4', SP3, 1, 1, 2,    r, false,   X1,    H, null,  7 ), // -C(X)(Y)-
	SS3_STEREO_X_L ( 'x', SP3, 1, 1, 2,    X, false,   X1,    H, null,  6 ), // -C(X)(Y)-
	SS3_CHIRAL_S_U ( '5', SP3, 1, 1, 2,    S, false,   X1,   X1, null, 10 ), // -C(X)(Y)-
	SS3_CHIRAL_R_U ( '6', SP3, 1, 1, 2,    R, false,   X1,   X1, null,  9 ), // -C(X)(Y)-
	SS3_CHIRAL_s_U ( '7', SP3, 1, 1, 2,    s, false,   X1,   X1, null, 10 ), // -C(X)(Y)-
	SS3_CHIRAL_r_U ( '8', SP3, 1, 1, 2,    r, false,   X1,   X1, null,  9 ), // -C(X)(Y)-
	SS3_CHIRAL_X_U ( 'X', SP3, 1, 1, 2,    X, false,   X1,   X1, null,  8 ), // -C(X)(Y)-
	SS2_KETONE_U   ( 'O', SP2, 1, 1, 1, null, false,   X2, null, null, 17 ), // -C(=X)-
	DS2_CISTRANS_EL( 'e', SP2, 2, 1, 1,    E, false,    H, null, null,  6 ), // =C(H)-
	DS2_CISTRANS_ZL( 'z', SP2, 2, 1, 1,    Z, false,    H, null, null,  5 ), // =C(H)-
	DS2_CISTRANS_NL( 'n', SP2, 2, 1, 1,    N, false,    H, null, null,  4 ), // =C(H)-
	DS2_CISTRANS_XL( 'f', SP2, 2, 1, 1,    X, false,    H, null, null,  4 ), // =C(H)-
	DS2_CISTRANS_EU( 'E', SP2, 2, 1, 1,    E, false,   X1, null, null,  8 ), // =C(X)-
	DS2_CISTRANS_ZU( 'Z', SP2, 2, 1, 1,    Z, false,   X1, null, null,  7 ), // =C(X)-
	DS2_CISTRANS_NU( 'N', SP2, 2, 1, 1,    N, false,   X1, null, null,  6 ), // =C(X)-
	DS2_CISTRANS_XU( 'F', SP2, 2, 1, 1,    X, false,   X1, null, null,  6 ), // =C(X)-
	DD1_KETENE     ( 'K', SP , 2, 2, 0, null, false, null, null, null,  4 ), // =C=
	TS1_ETHYNE     ( 'T', SP , 3, 1, 0, null, false, null, null, null,  4 ), // #C-
	// Anomeric
	SSX_ANOMER     ( 'a', SP3, 1, 1, 2, null,  true,   X1,   X1, null, 17 ), // -C(X)(Y)- at anomeric position
	// Ambiguous
	SSX_UNDEF_U    ( 'U', SPX, 1, 1,-1, null,  null, null, null, null, 17 ), // -C(X)(Y)- or -C(=X)-
	SSX_UNDEF_ALL  ( 'Q', SPX,-1,-1,-1, null,  null, null, null, null,  0 ), // non terminal carbon type undefined at all

	XXX_UNKNOWN    ( '?', SPX,-1,-1,-1, null,  null, null, null, null,  0 ); // C???

	/** Charactor of carbon descriptor */
	private char   m_cChar;
	/** Hybrid orbital */
	private String m_strHybridOrbital;
	/** Bond type of connecting first carbon */
	private int    m_iBondTypeCarbon1;
	/** Bond type of connecting second carbon (0 at terminal) */
	private int    m_iBondTypeCarbon2;
	/** Number of unique modifications */
	private int    m_nUniqueModification;
	/** Chirality or geometrical isomerism */
	private String m_strStereo;
	/** Whether or not the carbon is foot of bridge on the ring */
	private Boolean m_bIsFootOfBridge;
	/** ModificationType of first modificaiton */
	private ModificationType m_typeModification1;
	/** ModificationType of second modificaiton (or null) */
	private ModificationType m_typeModification2;
	/** ModificationType of third modificaiton (or null) */
	private ModificationType m_typeModification3;
	/** Score of carbon for comparison backbone */
	private int    m_iCarbonScore;

	/**
	 * Private constructor of CarbonDescriptor (a character of SkeltonCode)
	 * @param a_strChar SkeltonCode character of target carbon
	 * @param a_strOrbital Hybrid orbital of target carbon
	 * @param a_iTypeC1 Bond type of connection to first carbon
	 * @param a_iTypeC2 Bond type of connection to second carbon (0 at terminal carbon)
	 * @param a_nUniqMod Number of Unique modifications
	 * @param a_strStereo Stereo of target carbon
	 * @param a_bIsFoot Whether or not the carbon is foot of bridge on the ring
	 * @param a_strMod1 String of first modification
	 * @param a_strMod2 String of second modification (or null)
	 * @param a_strMod3 String of third modification (or null)
	 */
	private CarbonDescriptor( char a_strChar, String a_strOrbital,
			int a_iTypeC1, int a_iTypeC2, int a_nUniqMod,
			String a_strStereo, Boolean a_bIsFoot,
			ModificationType a_strMod1,
			ModificationType a_strMod2,
			ModificationType a_strMod3,
			int a_iScore ) {
		this.m_cChar             = a_strChar;
		this.m_strHybridOrbital    = a_strOrbital;
		this.m_iBondTypeCarbon1    = a_iTypeC1;
		this.m_iBondTypeCarbon2    = a_iTypeC2;
		this.m_nUniqueModification = a_nUniqMod;
		this.m_strStereo           = a_strStereo;
		this.m_bIsFootOfBridge     = a_bIsFoot;
		this.m_typeModification1    = a_strMod1;
		this.m_typeModification2    = a_strMod2;
		this.m_typeModification3    = a_strMod3;
		this.m_iCarbonScore        = a_iScore;
	}

	public char getChar() {
		return this.m_cChar;
	}

	public String getHybridOrbital() {
		return this.m_strHybridOrbital;
	}

	public int getBondTypeCarbon1() {
		return this.m_iBondTypeCarbon1;
	}

	public int getBondTypeCarbon2() {
		return this.m_iBondTypeCarbon2;
	}

	public Boolean isTerminal() {
		if ( this.m_cChar == '?' ) return null;
		return ( this.m_iBondTypeCarbon2 == 0 );
	}

	public int getNumberOfUniqueModifications() {
		return this.m_nUniqueModification;
	}

	public String getStereo() {
		return this.m_strStereo;
	}

	public Boolean isAnomeric() {
		return this.m_bIsFootOfBridge;
	}

	/** 
	 *  Whether or not the carbon is foot of bridge (null to false)
	 *  @deprecated Use {@link #isAnomeric()}
	 */
	public boolean isFootOfBridge() {
		return  this.m_bIsFootOfBridge == null ? false : this.m_bIsFootOfBridge;
	}

	public boolean isChiral() {
		if ( this.m_strHybridOrbital == null )
			return false;
		if ( !this.m_strHybridOrbital.equals(SP3) )
			return false;
		if ( this.m_strStereo == null )
			return false;
		return true;
	}

	public ModificationType getModificationType(int num) {
		if (num == 1) return this.m_typeModification1;
		if (num == 2) return this.m_typeModification2;
		if (num == 3) return this.m_typeModification3;
		return null;
	}

	public int getNumberOfModifications() {
		int num = 0;
		for ( int i=1; i<=3; i++ )
			if ( this.getModificationType(i) != null )
				num++;
		return num;
	}

	public int getNumberOfHydrogens() {
		int num = 0;
		for ( int i=1; i<=3; i++ )
			if ( H == this.getModificationType(i) )
				num++;
		return num;
	}

	public String getModification(int num) {
		ModificationType type = this.getModificationType(num);
		if ( type == null )
			return null;
		return type.toString();
	}

	/** Get score of the carbon for Backbone comparison */
	public int getCarbonScore() {
		return this.m_iCarbonScore;
	}

	/**
	 * Get CarbonDescriptor which correspond to a character of SkeltonCode
	 * @param cName A character of SkeltonCode
	 * @param isTerminal Whether or not the charactor is at terminal
	 * @return
	 */
	public static CarbonDescriptor forCharacter(char cName, Boolean isTerminal) {
		for ( CarbonDescriptor cd : CarbonDescriptor.values() ) {
			if ( cd.m_cChar != cName ) continue;
			if ( cd.isTerminal() != isTerminal ) continue;
			return cd;
		}
		return CarbonDescriptor.XXX_UNKNOWN;
	}

	/**
	 * Get CarbonDescriptor by carbon situation
	 * @param a_strOrbital
	 * @param a_iTypeC1
	 * @param a_iTypeC2
	 * @param a_nUniqMod
	 * @param a_strStereo
	 * @param a_bIsFoot
	 * @param a_strMod1
	 * @param a_strMod2
	 * @param a_strMod3
	 * @return
	 */
	public static CarbonDescriptor forCarbonSituation( String a_strOrbital,
			int a_iTypeC1, int a_iTypeC2, int a_nUniqMod,
			String a_strStereo, Boolean a_bIsFoot,
			ModificationType a_modType1,
			ModificationType a_modType2,
			ModificationType a_modType3
		) {
		// Match values
		for ( CarbonDescriptor cd : CarbonDescriptor.values() ) {
			if ( cd.m_strHybridOrbital != null
				&& !cd.m_strHybridOrbital.equals( a_strOrbital ) ) continue;
			if (  cd.m_iBondTypeCarbon1 != a_iTypeC1             ) continue;
			if (  cd.m_iBondTypeCarbon2 != a_iTypeC2             ) continue;
			if ( cd.m_nUniqueModification != -1
				&& cd.m_nUniqueModification != a_nUniqMod        ) continue;
			if ( cd.m_strStereo != null
				&& !cd.m_strStereo.equals( a_strStereo )         ) continue;
			if ( cd.m_bIsFootOfBridge != null
				&& !cd.m_bIsFootOfBridge.equals( a_bIsFoot )     ) continue;
			if ( !cd.matchModifications( a_modType1, a_modType2, a_modType3 ) ) continue;
			return cd;
		}
		return CarbonDescriptor.XXX_UNKNOWN;
	}

	private boolean matchModifications(
			ModificationType mod1,
			ModificationType mod2,
			ModificationType mod3
		) {

		// Set this modification strings to array
		ModificationType[] mMods = new ModificationType[3];
		mMods[0] = this.m_typeModification1;
		mMods[1] = this.m_typeModification2;
		mMods[2] = this.m_typeModification3;
		// Set argument modification strings to array
		ModificationType[] aMods = new ModificationType[3];
		aMods[0] = mod1;
		aMods[1] = mod2;
		aMods[2] = mod3;

		// Matching each modification
		for ( int i=0; i<3; i++ ) {
			if ( mMods[i] == null ) {
				if ( aMods[i] == null )
					return true;
				return false;
			}
			if ( aMods[i] == null )
				return false;
			if ( mMods[i] != aMods[i] )
				return false;
		}

		return true;
	}

	public CarbonDescriptor getInverted() {
		if ( !this.isChiral() )
			return this;
		if ( this.m_strStereo.equals("X") )
			return this;
		// Invert chirality
		String strStereoNew = (this.m_strStereo.equals("R"))? "S" :
			(this.m_strStereo.equals("S"))? "R" :
			(this.m_strStereo.equals("r"))? "s" :
			(this.m_strStereo.equals("s"))? "r" :
			this.m_strStereo;
		return forCarbonSituation(
				this.m_strHybridOrbital,
				this.m_iBondTypeCarbon1,
				this.m_iBondTypeCarbon2,
				this.m_nUniqueModification,
				strStereoNew,
				this.m_bIsFootOfBridge,
				this.m_typeModification1,
				this.m_typeModification2,
				this.m_typeModification3
			);
	}

	@Override
	public String toString() {
		return ""+this.m_cChar;
	}
}
