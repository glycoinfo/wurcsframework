package org.glycoinfo.WURCSFramework.wurcs.graph;

import java.util.LinkedList;

import org.glycoinfo.WURCSFramework.util.graph.visitor.WURCSVisitor;
import org.glycoinfo.WURCSFramework.util.graph.visitor.WURCSVisitorException;


/**
 * Class for backbone of monosaccharide
 * @author MasaakiMatsubara
 *
 */
public class Backbone extends WURCSComponent{

	/** BackboneCarbons  */
	private LinkedList<BackboneCarbon> m_aCarbons;
	/** BackboneCarbons  */
	private int  m_iAnomericPosition;
	private char m_iAnomericSymbol;

	public Backbone() {
		this.m_aCarbons = new LinkedList<>();
		this.m_iAnomericPosition = 0;
		this.m_iAnomericSymbol = 'o';
	}

	public int getAnomericPosition() {
		return this.m_iAnomericPosition;
	}

	public void setAnomericPosition(int a_iAnomPos) {
		this.m_iAnomericPosition = a_iAnomPos;
	}

	public char getAnomericSymbol() {
		return this.m_iAnomericSymbol;
	}

	public void setAnomericSymbol(char a_cAnomSymbol) {
		this.m_iAnomericSymbol = a_cAnomSymbol;
	}

	/**
	 * Add backbone carbon
	 * @param bc BackboneCarbon
	 * @return true if addition is succeed
	 */
	public boolean addBackboneCarbon( BackboneCarbon bc ) {
		if ( this.m_aCarbons.contains(bc) ) return false;
		return this.m_aCarbons.add( bc );
	}

	/**
	 * Get list of BackboneCarbon in this component
	 * @return list of BackboneCarbon in this component
	 */
	public LinkedList<BackboneCarbon> getBackboneCarbons() {
		return this.m_aCarbons;
	}

	/** Return carbon chain length or {@code -1} if containing backbone carbon with unknown length */
	public int getLength() {
		if ( this.hasUnknownLength() )
			return -1;
		return this.m_aCarbons.size();
	}

	/** Get SkeletonCode from BackboneCarbons */
	public String getSkeletonCode() {
		String code = "";
		for ( BackboneCarbon oBC : this.m_aCarbons ) {
			code += oBC.getCode();
		}
		return code;
	}

//	public int getAnomericPosition() {
//		if ( this.m_objAnomericCarbon == null ) return 0;
//		// For open chain
//		CarbonDescriptorOld CD = (CarbonDescriptorOld)this.m_objAnomericCarbon.getDesctriptor();
//		if ( ! CD.isFootOfBridge() ) return 0;
//		return this.getBackboneCarbons().indexOf(this.m_objAnomericCarbon)+1;
//	}

	/** Get anomeric symbol */
//	public char getAnomericSymbol() {
//		// Get configurational carbon
//		int pos = this.getAnomericPosition();
//		if ( pos == 0 ) return 'x';
//		int i = 0;
//		BackboneCarbon bcConfig = null;
//		for ( BackboneCarbon bc : this.getBackboneCarbons() ) {
//			if ( !bc.isChiral() ) continue;
//			i++;
//			bcConfig = bc;
//			if ( i == 5 ) break;
//		}
//
//		// Determine anomeric charactor
//		char anom = 'x';
//		if ( bcConfig == null ) return anom;
//		if ( this.m_objAnomericCarbon == null ) return anom;
//
//		char cConfig = bcConfig.getDesctriptor().getChar();
//		char cAnom = this.m_objAnomericCarbon.getDesctriptor().getChar();
//
//		if ( cConfig == 'x' || cConfig == 'X' ) {
//			return (cAnom == '3' || cAnom == '7')? 'b' :
//				   (cAnom == '4' || cAnom == '8')? 'a' : anom;
//		}
//		if ( !Character.isDigit(cConfig) ) return anom;
//		int iConfig = Character.getNumericValue(cConfig);
//
//		if ( !Character.isDigit(cAnom) ) return anom;
//		int iAnom = Character.getNumericValue(cAnom);
//
//		anom = ( iConfig%2 == iAnom%2 )? 'a' : 'b';
//
//		return anom;
//	}

	/**
	 * Get anomeric edge which is not fazzy and contain glycosidic linkage
	 * @return edge Edge on anomeric position
	 */
//	public WURCSEdge getAnomericEdge() {
//		if ( this.getAnomericPosition() == 0 ) return null;
//
//		for ( WURCSEdge edge : this.getEdges() ) {
//			if ( edge.getLinkages().size()>1 ) continue;
//			if ( edge.getLinkages().get(0).getBackbonePosition() != this.getAnomericPosition() ) continue;
//			if ( edge.getModification() == null ) continue;
//			if ( !edge.getModification().isGlycosidic() ) continue;
//			if ( edge.getModification() instanceof InterfaceRepeat ) continue;
//			return edge;
//		}
//		return null;
//	}

	/**
	 * Whether or not the backbone has parent
	 * @return
	 */
//	public boolean hasParent() {
//		WURCSEdge t_objAnomEdge = this.getAnomericEdge();
//		// If no anomeric position
//		if ( t_objAnomEdge == null ) return false;
//		// If anomeric position has glycosidic linkage
//		if ( t_objAnomEdge.getModification().isGlycosidic() ) return true;
//		// If anomeric modification is aglycone
//		if ( t_objAnomEdge.getModification().isAglycone() ) return false;
//
//		return true;
//	}

	public boolean hasUnknownLength() {
		for ( BackboneCarbon bc : this.m_aCarbons ) {
			if ( bc.hasUnknownLength() ) return true;
		}
		return false;
	}

	/**
	 * Calculate Backbone scores based on carbon score of CarbonDescriptor
	 * @return Integer of backbone score
	 */
//	public int getBackboneScore() {
//		int score = 0;
//		int length = this.m_aCarbons.size();
//		for ( int i=0; i<length; i++ )
//			score += (i+1) * this.m_aCarbons.get(i).getDesctriptor().getCarbonScore();
//		return score;
//	}

	private void clear() {
		this.m_aCarbons.clear();
//		this.m_objAnomericCarbon = null;
	}

	/**
	 * Copy
	 * @return copied backbone
	 */
//	public Backbone copy() {
//		Backbone copy = new Backbone();
//		for ( BackboneCarbon bc : this.m_aCarbons ) {
//			copy.addBackboneCarbon(bc.copy(copy));
//		}
//		copy.removeAllEdges();
//		// XXX Check needs for edge in copy
///*
//		for ( WURCSEdge edge : this.getEdges() ) {
//			if ( edge.getModification().isGlycosidic() ) continue;
//			WURCSEdge copyEdge = edge.copy();
//			copyEdge.setBackbone(copy);
//			copyEdge.setModification(edge.getModification().copy());
//			copy.addEdge(copyEdge);
//		}
//*/
//		return copy;
//	}

	/**
	 * Invert
	 * @return inverted backbone
	 */
//	public void invert() {
//		LinkedList<BackboneCarbon> inverts = new LinkedList<BackboneCarbon>();
//		for ( BackboneCarbon orig : this.m_aCarbons )
//			inverts.addFirst( orig.invert(this) );
//		this.clear();
//		for ( BackboneCarbon inv : inverts ) {
//			this.m_aCarbons.add(inv);
//			this.checkAnomeric(inv);
//		}
//		for ( WURCSEdge edge : this.getEdges() ) {
//			for ( LinkagePosition link : edge.getLinkages() ) {
//				link.invertBackbonePosition(this.m_aCarbons.size());
//			}
//		}
//	}

//	private void checkAnomeric(BackboneCarbon bc) {
//		// Set anomeric carbon
//		if ( ! bc.isAnomeric() ) return;
//		if ( this.m_objAnomericCarbon == null )
//			this.m_objAnomericCarbon = bc;
//		if ( this.m_objAnomericCarbon.getDesctriptor() == bc.getDesctriptor() ) return;
//
//		if ( this.m_objAnomericCarbon.getDesctriptor().getCarbonScore() - bc.getDesctriptor().getCarbonScore() < 0 )
//			this.m_objAnomericCarbon = bc;
//	}

	/**
	 * Get anomeric edge which is not fazzy
	 * @return edge Edge on anomeric position
	 */
	public WURCSEdge getAnomericEdge() {
		if ( this.m_iAnomericPosition == 0 ) return null;

		for ( WURCSEdge edge : this.getEdges() ) {
			if ( edge.getLinkages().size()>1 ) continue;
			if ( edge.getLinkages().get(0).getBackbonePosition() != this.m_iAnomericPosition ) continue;
			if ( edge.getModification() == null ) continue;
			if ( edge.getModification().isRing() ) continue;
			if ( edge.getModification() instanceof InterfaceRepeat ) continue;
			return edge;
		}
		return null;
	}

	/**
	 * Whether or not the backbone has parent
	 * @return
	 */
	public boolean hasParent() {
		WURCSEdge t_objAnomEdge = this.getAnomericEdge();
		// If no anomeric position
		if ( t_objAnomEdge == null ) return false;
		// If anomeric position has glycosidic linkage
		if ( t_objAnomEdge.getModification().isGlycosidic() ) return true;
		// If anomeric modification is aglycone
		if ( t_objAnomEdge.getModification().isAglycone() ) return false;

		return true;
	}

	/**
	 * Calculate Backbone scores based on carbon score of CarbonDescriptor
	 * @return Integer of backbone score
	 */
	public int getBackboneScore() {
		int score = 0;
		score += 20 * this.m_iAnomericPosition;
		int length = this.getLength();
		for ( int i=0; i<length; i++ )
			score += (i+1) * this.getBackboneCarbons().get(i).getDescriptor().getCarbonScore();
		return score;
	}

	/**
	 * Copy
	 * @return copied backbone
	 */
	public Backbone copy() {
		Backbone copy = new Backbone();
		for ( BackboneCarbon bc : this.getBackboneCarbons() ) {
			copy.addBackboneCarbon(bc.copy(copy));
		}
		copy.setAnomericPosition(this.m_iAnomericPosition);
		copy.setAnomericSymbol(this.m_iAnomericSymbol);
		copy.removeAllEdges();
		// XXX Check needs for edge in copy
/*
		for ( WURCSEdge edge : this.getEdges() ) {
			if ( edge.getModification().isGlycosidic() ) continue;
			WURCSEdge copyEdge = edge.copy();
			copyEdge.setBackbone(copy);
			copyEdge.setModification(edge.getModification().copy());
			copy.addEdge(copyEdge);
		}
*/
		return copy;
	}

	/**
	 * Invert
	 */
	public void invert() {
		// Do not invert for unknown backbone
		if ( this.hasUnknownLength() )
			return;

		LinkedList<BackboneCarbon> inverts = new LinkedList<BackboneCarbon>();
		for ( BackboneCarbon orig : this.m_aCarbons )
			inverts.addFirst( orig.invert(this) );
		this.clear();
		for ( BackboneCarbon inv : inverts ) {
			this.m_aCarbons.add(inv);
//			this.checkAnomeric(inv);
		}
		for ( WURCSEdge edge : this.getEdges() ) {
			for ( LinkagePosition link : edge.getLinkages() ) {
				link.invertBackbonePosition(this.m_aCarbons.size());
			}
		}
		// Invert anomeric position
		if ( this.m_iAnomericPosition > 0 && !this.hasUnknownLength() )
			this.m_iAnomericPosition = this.getLength() + 1 - this.m_iAnomericPosition;
	}

	public void accept(WURCSVisitor a_objVisitor) throws WURCSVisitorException {
		a_objVisitor.visit(this);
	}

	public String toString() {
		String strCode = this.getSkeletonCode();
		if ( this.m_iAnomericPosition != 0 && this.m_iAnomericSymbol != 'o' ) {
			strCode += "-";
			strCode += (this.m_iAnomericPosition < 0)? "?" : this.m_iAnomericPosition;
			strCode += this.m_iAnomericSymbol;
		}
		return strCode;
	}
}
