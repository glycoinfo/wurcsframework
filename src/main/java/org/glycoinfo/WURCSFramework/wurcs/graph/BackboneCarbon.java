package org.glycoinfo.WURCSFramework.wurcs.graph;

/**
 * Class for a carbon in backbone
 * @author MasaakiMatsubara
 *
 */
public class BackboneCarbon {
	/** Backbone which contain this*/
	private Backbone m_objBackbone;
	/** Descriptor for this carbon */
	private ICarbonDescriptor m_objCarbonDescriptor;
	/** Whether or not the carbon has unkown length */
	private boolean m_bHasUnknownLength;

	/**
	 * Constructor
	 * @param a_oBackbone
	 * @param a_enumCarbonDescriptor
	 */
	public BackboneCarbon(Backbone a_oBackbone, ICarbonDescriptor a_enumCarbonDescriptor) {
		this(a_oBackbone, a_enumCarbonDescriptor, false);
	}

	/**
	 * Constructor 
	 * @param a_oBackbone
	 * @param a_enumCarbonDescriptor
	 * @param a_bHasUnknownLength
	 */
	public BackboneCarbon(Backbone a_oBackbone, ICarbonDescriptor a_enumCarbonDescriptor, boolean a_bHasUnknownLength) {
		this.m_objBackbone = a_oBackbone;
		this.m_objCarbonDescriptor = a_enumCarbonDescriptor;
		this.m_bHasUnknownLength = a_bHasUnknownLength;
	}

	public Backbone getBackbone() {
		return this.m_objBackbone;
	}

	public ICarbonDescriptor getDescriptor() {
		return this.m_objCarbonDescriptor;
	}

	public boolean hasUnknownLength() {
		return this.m_bHasUnknownLength;
	}

	public boolean isChiral() {
		return this.m_objCarbonDescriptor.isChiral();
	}

	public String getCode() {
		String strCode = ""+this.m_objCarbonDescriptor.getChar();
		if ( this.hasUnknownLength() )
			strCode = "<"+strCode+">";
		return strCode;
	}

	/**
	 * Clone
	 * @param backbone
	 * @return clone backbone carbon
	 */
	public BackboneCarbon copy(Backbone backbone) {
		return new BackboneCarbon(backbone, this.m_objCarbonDescriptor, this.m_bHasUnknownLength);
	}

	/**
	 * Invert
	 * @param backbone
	 * @return inverted backbone carbon
	 */
	public BackboneCarbon invert(Backbone backbone) {
		return new BackboneCarbon(backbone, this.m_objCarbonDescriptor.getInverted(), this.m_bHasUnknownLength);
	}

	@Override
	public String toString() {
		return this.getCode();
	}
}
