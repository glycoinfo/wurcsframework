package org.glycoinfo.WURCSFramework.wurcs.graph;

public interface ICarbonDescriptor {

	public static enum ModificationType {
		H("-H", 1, true),
		X1("-X", 1, false),
		X2("=X", 2, false),
		X3("#X", 3, false);

		private String m_strSymbol;
		private int m_iOrder;
		private boolean m_bIsHydrogen;

		private ModificationType(String strSymbol, int iOrder, boolean bIsHydrogen) {
			this.m_strSymbol = strSymbol;
			this.m_iOrder = iOrder;
			this.m_bIsHydrogen = bIsHydrogen;
		}

		public String getSymbol() {
			return this.m_strSymbol;
		}

		public int getOrder() {
			return this.m_iOrder;
		}

		public boolean isHydrogen() {
			return this.m_bIsHydrogen;
		}

		public static ModificationType forSymbol(String a_strSymbol) {
			for ( ModificationType type : ModificationType.values() )
				if ( type.m_strSymbol.equals(a_strSymbol) )
					return type;
			return null;
		}

		public static ModificationType forConnection(int a_iOrder, boolean a_bIsHydrogen) {
			for ( ModificationType type : ModificationType.values() ) {
				if ( type.m_iOrder != a_iOrder )
					continue;
				if ( type.m_bIsHydrogen != a_bIsHydrogen )
					continue;
				return type;
			}
			return null;
		}

		@Override
		public String toString() {
			return this.m_strSymbol;
		}
	}

	// For static modification type
	public static final ModificationType H  = ModificationType.H;
	public static final ModificationType X1 = ModificationType.X1;
	public static final ModificationType X2 = ModificationType.X2;
	public static final ModificationType X3 = ModificationType.X3;

	// For static hybrid orbital type
	public static final String SP  = "sp";
	public static final String SP2 = "sp2";
	public static final String SP3 = "sp3";
	public static final String SPX = "?";

	// For static stereochemistry type
	public static final String R = "R";
	public static final String S = "S";
	public static final String r = "r";
	public static final String s = "s";
	public static final String X = "X";
	public static final String E = "E";
	public static final String Z = "Z";
	public static final String N = "N";

	/** Returns character of the CarbonDescriptor */
	public char getChar();

	/** Returns String of hybrid orbital of the carbon */
	public String getHybridOrbital();

	/** Returns this is for terminal carbon or not */
	public Boolean isTerminal();

	/** Returns this is anomeric carbon or not */
	public Boolean isAnomeric();

	/** Returns number of unique modifications connected to this */
	public int getNumberOfUniqueModifications();

	/** Returns String of stereo "sp", "sp2" or "sp3". Returns {@code null} if no stereo. */
	public String getStereo();

	/** Returns this is chiral carbon or not */
	public boolean isChiral();

	/**
	 * Returns modification type
	 * @param num ID # of the modification
	 * @return ModificationType of the modification
	 */
	public ModificationType getModificationType(int num);

	/**
	 * Returns string of modification type
	 * @param num ID # of the modification
	 * @return String of the modification
	 */
	public String getModification(int num);

	/** Returns number of connecting modifications (containing hydrogens) */
	public int getNumberOfModifications();

	/** Returns number of connecting hydrogens */
	public int getNumberOfHydrogens();

	/** Returns score of the carbon for comparing Backbones */
	public int getCarbonScore();

	/** Returns inverted CarbonDescriptor */
	public ICarbonDescriptor getInverted();
}
