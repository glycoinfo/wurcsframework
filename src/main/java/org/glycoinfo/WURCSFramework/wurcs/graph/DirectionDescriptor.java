package org.glycoinfo.WURCSFramework.wurcs.graph;

public enum DirectionDescriptor {

	N('n',  null, 0), // Omittable from CarbonDescriptor
	U('u', "sp3", 1), // First (uno or up)
	D('d', "sp3", 2), // Second (dos or down)
	T('t', "sp3", 3), // Third (tres) for terminal chiral carbon
	E('e', "sp2", 4), // Trans (entgegen)
	Z('z', "sp2", 5), // Cis (zusammen)
	X('x',  null, 6), // Unknown chiral or unknown geometrical isomerism
	L(' ',  null, 7); // Compressed

	private char m_cSymbol;
	private String m_strOrbital;
	private int  m_iScore;

	private DirectionDescriptor(char a_cDirection, String a_strOrbital, int a_iScore) {
		this.m_cSymbol = a_cDirection;
		this.m_strOrbital = a_strOrbital;
		this.m_iScore = a_iScore;
	}

	public char getName() {
		return this.m_cSymbol;
	}

	public String getOrbital() {
		return this.m_strOrbital;
	}

	public int getScore() {
		return this.m_iScore;
	}

	public static DirectionDescriptor forChar(char a_cDirection) {
		for ( DirectionDescriptor DD : DirectionDescriptor.values() ) {
			if ( DD.m_cSymbol == a_cDirection ) return DD;
		}
		return null;
	}
}
