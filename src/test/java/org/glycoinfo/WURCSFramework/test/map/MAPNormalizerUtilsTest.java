package org.glycoinfo.WURCSFramework.test.map;

import static org.junit.Assert.assertEquals;

import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;
import org.glycoinfo.WURCSFramework.util.map.MAPGraphExporter;
import org.glycoinfo.WURCSFramework.util.map.MAPGraphImporter;
import org.glycoinfo.WURCSFramework.util.map.analysis.MAPNormalizerUtils;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPGraph;
import org.junit.Test;

public class MAPNormalizerUtilsTest {

	private interface Manipulater {
		public void manipulate(MAPGraph graph);
	}

	@Test
	public void testRemoveStereoOnAromatic() {
		String input  = "*N(C^EC^ZC^ZC^ZC^ZN^E$3)";
		String output = "*N(CCCCCN$3)";

		String result = process(input, (graph) ->
			MAPNormalizerUtils.removeStereoOnAromatic(graph)
		);
		assertEquals(output, result);
	}

	public String process(String a_strMAP, Manipulater a_oManipulator) {
		try {
			MAPGraph t_oGraph = MAPGraphImporter.parseMAP(a_strMAP);
			a_oManipulator.manipulate(t_oGraph);
			return MAPGraphExporter.getMAP(t_oGraph);
		} catch (WURCSFormatException e) {
			return null;
		}
	}
}
