package org.glycoinfo.WURCSFramework.test.map;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.Map;

import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;
import org.glycoinfo.WURCSFramework.util.map.MAPGraphImporter;
import org.glycoinfo.WURCSFramework.util.map.analysis.MAPGraphNormalizer;
import org.glycoinfo.WURCSFramework.util.map.analysis.MAPStarComparator;
import org.glycoinfo.WURCSFramework.util.map.analysis.MorganAlgorithmForMAP;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPAtomAbstract;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPAtomCyclic;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPGraph;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPStar;
import org.junit.Test;

public class MorganAlgorithmForMAPTest {

	@Test
	public void test() throws WURCSFormatException {
		String t_strImputMAP = //"*NCCO*";
		"*NSN*/4C(CCCCCCCCS$11/10CC$7)/3=O/3=O/2C(CCCCCC$21)";
		MAPGraph t_oGraph = MAPGraphImporter.parseMAP(t_strImputMAP);

		MorganAlgorithmForMAP t_oMA1 = new MorganAlgorithmForMAP(t_oGraph);
		t_oMA1.calcMorganNumber(null, null);
		Map<MAPAtomAbstract, Integer> m_mapAtomToMorganNumber1 = t_oMA1.getAtomToMorganNumber();

		MAPGraphNormalizer norm = new MAPGraphNormalizer(t_oGraph);
		MAPGraph t_oGraphNoParent = norm.copyGraphWithNoParentConnections(t_oGraph);

		MorganAlgorithmForMAP t_oMA2 = new MorganAlgorithmForMAP(t_oGraphNoParent);
		t_oMA2.calcMorganNumber(null, null);
		Map<MAPAtomAbstract, Integer> m_mapAtomToMorganNumber2 = t_oMA2.getAtomToMorganNumber();

		for ( MAPAtomAbstract atom : t_oGraph.getAtoms() ) {
			if ( atom instanceof MAPAtomCyclic )
				continue;
			int num1 = m_mapAtomToMorganNumber1.get(atom);
			int num2 = m_mapAtomToMorganNumber2.get( norm.getCopiedAtom(atom) );
			System.out.println(num1+" : "+num2);
		}

		LinkedList<MAPStar> t_aStars = t_oGraph.getStars();
		MAPStar t_oStar11 = t_aStars.getFirst();
		MAPStar t_oStar12 = t_aStars.getLast();
		int t_iStar11 = m_mapAtomToMorganNumber1.get(t_oStar11);
		int t_iStar12 = m_mapAtomToMorganNumber1.get(t_oStar12);

		MAPStar t_oStar21 = (MAPStar) norm.getCopiedAtom(t_oStar11);
		MAPStar t_oStar22 = (MAPStar) norm.getCopiedAtom(t_oStar12);
		int t_iStar21 = m_mapAtomToMorganNumber2.get(t_oStar21);
		int t_iStar22 = m_mapAtomToMorganNumber2.get(t_oStar22);

		assertTrue( t_iStar11 == t_iStar21 );
		assertTrue( t_iStar12 == t_iStar22 );
	}

}
