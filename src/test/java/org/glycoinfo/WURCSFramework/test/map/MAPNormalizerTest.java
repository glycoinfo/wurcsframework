package org.glycoinfo.WURCSFramework.test.map;

import static org.junit.Assert.*;

import org.glycoinfo.WURCSFramework.util.map.MAPNormalizer;
import org.junit.Test;

public class MAPNormalizerTest {

	@Test
	public void testAromaticBranch() {
		String input = "*O(CCCCCC$3)/5O/7O";
		String expected = "*O(CCCCCC$3/5)O(/7)O";

		// Normalize MAP
		assertEquals(expected, normalize(input));
	}

	@Test
	public void testStereoOnAromatic() {
		String input = "*N(C^EC^ZC^ZC^ZC^ZN^E$3)";
		String expected = "*N(CCCCCN$3)";

		// Normalize MAP
		assertEquals(expected, normalize(input));
	}

	private String normalize(String input) {
		MAPNormalizer normalizer = new MAPNormalizer(input);
		try {
			normalizer.start();
		} catch (Exception e) {
		}
		return normalizer.getOutput();
	}

}
