package org.glycoinfo.WURCSFramework.test.map;

import static org.junit.Assert.*;

import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;
import org.glycoinfo.WURCSFramework.util.map.MAPGraphImporter;
import org.junit.Test;

public class MAPGraphImporterTest {

	@Test
	public void testExceptions_parseMAP() {
		// Wrong formats
		testExceptionMessage("*N2",
				"Any numbers must not be followed by any atoms.");
		testExceptionMessage("*C=^C",
				"\"C\" is not stereochemical symbol.");
		testExceptionMessage("*C=^",
				"A stereochemical symbol must be followed by \"^\".");
		testExceptionMessage("*C=",
				"An atom must be followed by any bond symbol.");
		testExceptionMessage("*C=^X",
				"An atom must be followed by any bond symbol.");
		testExceptionMessage("*CC^RCC/3^SCC",
				"The different stereochemistries are set to the same atom: R vs S");
		testExceptionMessage("*CCCCCC$6^X",
				"The stereochemical symbol must be followed by an atom or a double bond.");
		testExceptionMessage("*NCC//3=O",
				"An index number for branched atom must be followed by \"/\".");
		testExceptionMessage("*NCC/5=O",
				"No such branched atom with index number \"5\".");
		testExceptionMessage("*CCCCCC$",
				"An index number for cyclic atom must be followed by \"$\".");
		testExceptionMessage("*CCCCCC$8",
				"No such cyclic atom with index number \"8\".");
		testExceptionMessage("*c",
				"Any atom symbol must start with upper case.");
		char[] symbols = { '!', '~', '&', '%', '`', '\'', '\"', ':', ';', '<', '>',
				'[', ']', '?', ',', '.', '_', '|', '\\' };
		for ( char cSymbol : symbols )
			testUndefinedSymbols(cSymbol);

		// Correct formats
		try {
			MAPGraphImporter.parseMAP("*@");
		} catch (WURCSFormatException e) {
			fail();
		}
	}

	private void testUndefinedSymbols(char a_cSymbol) {
		String strMAP = "*"+a_cSymbol;
		String strExpectedMessage = "The symbol \""+a_cSymbol+"\" is not defined.";
		testExceptionMessage(strMAP, strExpectedMessage);
	}

	private void testExceptionMessage(String a_strMAP, String a_strExpectedMessage) {
		try {
			MAPGraphImporter.parseMAP(a_strMAP);
			fail(a_strMAP);
		} catch (WURCSFormatException e) {
			assertEquals( a_strExpectedMessage, e.getMessage() );
		}
	}
}
