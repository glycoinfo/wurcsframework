package org.glycoinfo.WURCSFramework.test.map;

import static org.junit.Assert.*;

import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;
import org.glycoinfo.WURCSFramework.util.map.MAPGraphExporter;
import org.glycoinfo.WURCSFramework.util.map.MAPGraphImporter;
import org.glycoinfo.WURCSFramework.util.map.analysis.MAPGraphNormalizer;
import org.glycoinfo.WURCSFramework.util.map.analysis.cip.MAPStereochemistryCheckerUsingCIPSystem;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPAtomAbstract;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPAtomCyclic;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPConnection;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPGraph;
import org.junit.Test;

public class MAPGraphNormalizerTest {

	// TODO: Do not reset stereo for N+
//	@Test
//	public void testStereoChange() throws WURCSFormatException {
//		String t_strInputMAP = "*1N^R*2/2*3/2O";
//		MAPGraph t_oInputGraph = (new MAPGraphImporter()).parseMAP(t_strInputMAP);
//		MAPStereochemistryCheckerUsingCIPSystem t_oStereoCheck = new MAPStereochemistryCheckerUsingCIPSystem(t_oInputGraph);
//		t_oStereoCheck.resetStereo();
//		assertTrue(t_oStereoCheck.hasStereoChange());
//	}

	@Test
	public void testNormalize() throws WURCSFormatException {
		String t_strIdealMAP = "*1NSN*2/4C(CCCCCC$7)/3=O/3=O/2C(CCCCCCCCS$20/19CC$16)";
		String t_strInputMAP = "*NSN*/4C(CCCCCCCCS$11/10CC$7)/3=O/3=O/2C(CCCCCC$21)";

		MAPGraph t_oInputGraph = MAPGraphImporter.parseMAP(t_strInputMAP);
		MAPGraphNormalizer t_oNorm = new MAPGraphNormalizer(t_oInputGraph);

		// Result graph is null before start
		assertNull(t_oNorm.getNormalizedGraph());

		// Start normalize
		t_oNorm.start();

		// Result graph is not null after start
		assertNotNull(t_oNorm.getNormalizedGraph());

		MAPGraph t_oOutputGraph = t_oNorm.getNormalizedGraph();
		String t_strOutputMAP = MAPGraphExporter.getMAP(t_oOutputGraph);
		// Ideal and output MAPs must be the same string in this case
		assertEquals(t_strIdealMAP, t_strOutputMAP);
	}

	@Test
	public void testMAPGraphCopy() throws WURCSFormatException {
		String t_strInputMAP = "*OC(C^EC^ZC^ZC^ZC^ZC^ZC^ZC^Z$6/7^ZC^ZC^E$4)/3=O";
		MAPGraph t_oInputGraph = MAPGraphImporter.parseMAP(t_strInputMAP);
		MAPGraphNormalizer t_oNorm = new MAPGraphNormalizer(t_oInputGraph);

		MAPGraph t_oGraph = t_oNorm.copyGraphWithNoParentConnections(t_oInputGraph);
		for ( MAPAtomAbstract t_oAtom : t_oGraph.getAtoms() ) {
			// Error if there are parent connections
			assertNull( t_oAtom.getParentConnection() );
			// Error if there is MAPAtomCyclic
			assertFalse( t_oAtom instanceof MAPAtomCyclic );
			for ( MAPConnection t_oConn : t_oAtom.getChildConnections() ) {
				// Error if there is no reverse connection
				assertNotNull( t_oConn.getReverse() );
			}
		}

	}

}
