package org.glycoinfo.WURCSFramework.test.validation;

import org.glycoinfo.WURCSFramework.util.validation.WURCSValidator;

public class WURCSValidatorTestSimple {

	public static void main(String[] args) {
		for (String strWURCS : args)
			checkWURCS(strWURCS);
	}

	private static void checkWURCS(String a_strWURCS) {
		WURCSValidator validator=new WURCSValidator();
		validator.start(a_strWURCS);

		System.out.println("Input:");
		System.out.println(a_strWURCS);
		System.out.println("Resutls:");
		System.out.println(validator.getReport().getResults());
//		System.out.println(validator.getReport().getResultsSimple());
		System.out.println("Normalized:");
		System.out.println(validator.getReport().getStandardString());
		System.out.println();
	}

}
