package org.glycoinfo.WURCSFramework.test.validation;

import org.glycoinfo.WURCSFramework.util.validation.WURCSValidator;

public class WURCSValidatorTest {
	public static void main(String[] args) {
		// format
		checkWURCS("WURCS=2.0/2,2,1/[a2112h-1x_1-5_2*NCC//!3=O_4*OSO/3=O/3=O][a21eEA-1a_1-5]/1-2/a3-b1",
				"wrong format in MAP");

		// Anomer
		checkWURCS("WURCS=2.0/1,1,0/[a2122h-1a_1-5]/1/", "correct anomeric info and ring");
		checkWURCS("WURCS=2.0/1,1,0/[a2122h-1a_1-?]/1/", "correct anomeric info and ring");
		checkWURCS("WURCS=2.0/1,1,0/[a2122h-1q_1-5]/1/", "wrong anomeric symbol");
		checkWURCS("WURCS=2.0/1,1,0/[a2122h-2a_1-5]/1/", "wrong anomeric position");
		checkWURCS("WURCS=2.0/1,1,0/[a2122h-?a_1-5]/1/", "wrong anomeric position (unknown)");
		checkWURCS("WURCS=2.0/1,1,0/[a2122h-1a_2-6]/1/", "wrong ring for anomer");
		checkWURCS("WURCS=2.0/1,1,0/[a2122h-1a]/1/",     "no ring for anomer");
		checkWURCS("WURCS=2.0/1,1,0/[a2122h-0o]/1/",     "wrong anomeric info");
		checkWURCS("WURCS=2.0/1,1,0/[a2122h]/1/",        "no anomeric info and ring");
		checkWURCS("WURCS=2.0/1,1,0/[u2122h]/1/",        "correct potential anomer");
		checkWURCS("WURCS=2.0/1,1,0/[u2122h-1a_1-5]/1/", "wrong anomer info and ring for potential anomer");
		checkWURCS("WURCS=2.0/1,1,0/[u2122h-1a]/1/",     "wrong anomer info for potential anomer");
		checkWURCS("WURCS=2.0/1,1,0/[o2122h]/1/",        "correct open chain");
		checkWURCS("WURCS=2.0/1,1,0/[o2122h-1a]/1/",     "wrong anomer info");
		checkWURCS("WURCS=2.0/1,1,0/[o2122h-1a_1-5]/1/", "wrong anomer info and ring");
		checkWURCS("WURCS=2.0/1,1,0/[<Q>]/1/",           "correct unknown backbone");
		checkWURCS("WURCS=2.0/1,1,0/[<Q>-1a_1-?]/1/",    "correct anomer info and ring for unknown backbone");
		checkWURCS("WURCS=2.0/1,1,0/[<Q>-1a]/1/",        "no ring for anomer");
		checkWURCS("WURCS=2.0/1,1,0/[<Q>-0o]/1/",        "correct unknown backbone with direct specified open chain");
		checkWURCS("WURCS=2.0/1,1,0/[<Q>-?o]/1/",        "wrong unknown backbone with direct specified open chain");
		checkWURCS("WURCS=2.0/1,1,0/[<Q>-?a]/1/",        "correct anomer info without position for unknown backbone");
		checkWURCS("WURCS=2.0/1,1,0/[a2a22h-1a_1-5]/1/", "too many anomeric CDs, but correct anomeric info");
		checkWURCS("WURCS=2.0/1,1,0/[a2a22h-1a]/1/",     "too many anomeric CDs, and no ring for anomer");
		checkWURCS("WURCS=2.0/1,1,0/[a2a22h]/1/",        "too many anomeric CDs, and no anomeric info");
		checkWURCS("WURCS=2.0/1,1,0/[a2U22h-1a_1-5]/1/", "wrong combination of anomeric and potential anomeric CDs, but correct anomeric info");
		checkWURCS("WURCS=2.0/1,1,0/[a2O22h-1a_1-5]/1/", "correct combination of anomeric and carbonyl CDs, and correct anomeric info");
		checkWURCS("WURCS=2.0/1,1,0/[u2U22h]/1/",        "correct combination of potential anomeric CDs");

		checkWURCS("WURCS=2.0/1,1,0/[o2O22h]/1/",        "subsumed by u2U22h");
		checkWURCS("WURCS=2.0/1,1,0/[a2O22h-1x_1-?]/1/", "subsumed by u2U22h");
		checkWURCS("WURCS=2.0/1,1,0/[o2a22h-3x_3-?]/1/", "subsumed by u2U22h");

		checkWURCS("WURCS=2.0/1,2,2/[a2122h-1a_1-5]/1-1/a4-b1_a1-a4~n", "correct repeat");
		checkWURCS("WURCS=2.0/1,2,2/[a2122h-1a_1-5]/1-1/a4-b1_b1-b4~n", "correct repeat");
		checkWURCS("WURCS=2.0/1,2,2/[a2122h-1a_1-5]/1-1/a4-b1_a1-a4~1-5", "correct repeat");
		checkWURCS("WURCS=2.0/1,2,2/[a2122h-1a_1-5]/1-1/a4-b1_a1-a4~5-1", "correct repeat (to be normalized)");

		checkWURCS("WURCS=2.0/1,4,3/[a2122h-1a_1-5]/1-1-1-1/a4-b1_b4-c1_d1-a6|b6|c6}", "correct fragment");
		checkWURCS("WURCS=2.0/5,7,6/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a1221m-1a_1-5]/1-2-3-4-4-5-4/a4-b1_a6-f1_b4-c1_c3-d1_c6-e1_g1-d?|e?}", "correct fragment");
		checkWURCS("WURCS=2.0/5,11,10/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a1122h-1x_1-5]/1-2-3-4-4-5-5-5-5-5-5/a4-b1_b4-c1_c3-d1_c6-e1_f?-a?|b?|c?|d?|e?}_g?-a?|b?|c?|d?|e?}_h?-a?|b?|c?|d?|e?}_i?-a?|b?|c?|d?|e?}_j?-a?|b?|c?|d?|e?}_k?-a?|b?|c?|d?|e?}", "warning fragment");

		checkWURCS("WURCS=2.0/1,1,0/[o2122h_8*NCC/3=O]/1/", "wrong position of mod");
		checkWURCS("WURCS=2.0/1,1,0/[o2122h_1*NCC/3=O]/1/", "wrong connection to CD");
		checkWURCS("WURCS=2.0/1,1,0/[o2122h_1*=N]/1/",      "correct connection to CD");
		checkWURCS("WURCS=2.0/1,1,0/[o2122h_3_4]/1/",       "wrong mod (no MAP)");
		checkWURCS("WURCS=2.0/1,1,0/[o2122h_2*O]/1/",       "correct mod (to be omitted)");
		checkWURCS("WURCS=2.0/1,1,0/[o2122h_2*=N]/1/",      "wrong connection to CD");
		checkWURCS("WURCS=2.0/1,1,0/[o212?h]/1/",           "unknown CD contains");

		checkWURCS("WURCS=2.0/1,1,0/[o2122c]/1/",                      "correct mods on CD (two omitted *O)");
		checkWURCS("WURCS=2.0/1,1,0/[o2122c_6u*OC]/1/",                "correct mods and directions on CD (one omitted *O)");
		checkWURCS("WURCS=2.0/1,1,0/[o2122c_6u*OC_6d*OCC]/1/",         "correct mods and directions on CD");
		checkWURCS("WURCS=2.0/1,1,0/[o2122c_6u*OC_6d*OCC_6t*OCCC]/1/", "wrong mods on CD (too many modifications)");
		checkWURCS("WURCS=2.0/1,1,0/[o2122c_6*OC]/1/",                 "wrong direction on CD (one omitted *O)");
		checkWURCS("WURCS=2.0/1,1,0/[o2122M]/1/",                      "correct directions on CD (three omitted *O)");
		checkWURCS("WURCS=2.0/1,1,0/[o2122M_6u*OC]/1/",                "correct directions on CD (two omitted *O)");
		checkWURCS("WURCS=2.0/1,1,0/[o2122M_6u*OC_6d*OCC]/1/",         "correct directions on CD (one omitted *O)");
		checkWURCS("WURCS=2.0/1,1,0/[o2122M_6u*OC_6d*OCC_6t*OCCC]/1/", "correct directions on CD");
		checkWURCS("WURCS=2.0/1,1,0/[o2122M_6*OC_6*OCC_6*OCCC]/1/",    "wrong directions on CD");
		checkWURCS("WURCS=2.0/1,1,0/[o2122C_6*N]/1/",                "correct mods on CD (two omitted *O)");
		checkWURCS("WURCS=2.0/1,1,0/[o2122C_6u*OC_6*N]/1/",          "correct mods and directions on CD (one omitted *O)");
		checkWURCS("WURCS=2.0/1,1,0/[o2122C_6u*OC_6d*OCC_6*N]/1/",   "correct mods and directions on CD");
		checkWURCS("WURCS=2.0/1,1,0/[o2122C_6*OC_6*OCC_6*N]/1/",     "wrong directions on CD (direction needs for *O...)");
		checkWURCS("WURCS=2.0/1,1,0/[o2122C_6u*OC_6*OCC_6*N]/1/",    "wrong directions on CD (direction needs for *O...)");
		checkWURCS("WURCS=2.0/1,1,0/[o212nN]/1/",              "correct directions on CD (two omitted *O)");
		checkWURCS("WURCS=2.0/1,1,0/[o212nN_6e*OC]/1/",        "correct directions on CD (one omitted *O)");
		checkWURCS("WURCS=2.0/1,1,0/[o212nN_6*OC]/1/",         "wrong directions on CD (one omitted *O)");
		checkWURCS("WURCS=2.0/1,1,0/[o212nN_6e*OC_6z*OCC]/1/", "correct directions on CD");
		checkWURCS("WURCS=2.0/1,1,0/[o212fF_6e*OC_6z*OCC]/1/", "wrong directions on CD");

		checkWURCS("WURCS=2.0/1,1,0/[hxeEZz]/1/", "correct backbone carbon connections");
		checkWURCS("WURCS=2.0/1,1,0/[hxFfnN]/1/", "correct backbone carbon connections");
		checkWURCS("WURCS=2.0/1,1,0/[hxTTnN]/1/", "correct backbone carbon connections");
		checkWURCS("WURCS=2.0/1,1,0/[tTeKeh]/1/", "correct backbone carbon connections");
		checkWURCS("WURCS=2.0/1,1,0/[KKKNxh]/1/", "correct backbone carbon connections");
		checkWURCS("WURCS=2.0/1,1,0/[hxezfn]/1/", "wrong backbone carbon connections");
		checkWURCS("WURCS=2.0/1,1,0/[hxexfh]/1/", "wrong backbone carbon connections");

		checkWURCS("WURCS=2.0/1,1,0/[Aad21122h-2a_2-6]/1/",            "correct KDN");
		checkWURCS("WURCS=2.0/1,1,0/[Aad21122h-2a_2-6_5*O]/1/",        "correct KDN, but MAP is not omitted");
		checkWURCS("WURCS=2.0/1,1,0/[Aad21122h-2a_2-6_5*N]/1/",        "correct Neu");
		checkWURCS("WURCS=2.0/1,1,0/[Aad21122h-2a_2-6_5*NCC/3=O]/1/",  "correct Neu5Ac");
		checkWURCS("WURCS=2.0/1,1,0/[Aad21122h-2a_2-6_5*NCCO/3=O]/1/", "correct Neu5Gc");
		checkWURCS("WURCS=2.0/1,1,0/[Aad21122h-2a_2-6_5NCC/3=O]/1/",   "wrong Neu5Ac without *");

		checkWURCS("WURCS=2.0/1,3,1/[Aad21122h-2a_2-6]/1-1-1/a5-b2-c2*N*/2*",  "correct # of connections on N");
		checkWURCS("WURCS=2.0/1,2,1/[Aad21122h-2a_2-6]/1-1/a5-b2*N*/2*",       "wrong # of connections on N");
		checkWURCS("WURCS=2.0/1,2,1/[Aad21122h-2a_2-6_5*N]/1-1/a5-b2",         "wrong # of connections on position 5");
		checkWURCS("WURCS=2.0/1,2,1/[Aad21122h-2a_2-6]/1-1/a5n1-b2n2*1NCCO*2", "correct connections on asymmetric MAP");
		checkWURCS("WURCS=2.0/1,2,1/[Aad21122h-2a_2-6]/1-1/a5n3-b2n4*1NCCO*2", "wrong connections on asymmetric MAP (wrong StarIndices on the linkages)");
		checkWURCS("WURCS=2.0/1,2,1/[Aad21122h-2a_2-6]/1-1/a5-b2*1NCCO*2",     "wrong connections on asymmetric MAP (needs StarIndices on the linkages)");
		checkWURCS("WURCS=2.0/1,2,1/[Aad21122h-2a_2-6]/1-1/a8-b2*O*",          "correct glycosidic linkage, but MAP is not omitted");

		checkWURCS("WURCS=2.0/2,2,2/[Ad1dd22h_3-7_1*NC/2C][h2h]/1-2/a6-b3*N*/2C(CC^ZCCCC$5)/8CF/11F/11F_a8-b1", "wrong MAP");

		checkWURCS("WURCS=2.0/2,6,5/[o222h][a222h-1x_1-4]/1-2-2-2-2-2/c1-d1_e1-f1_a?-b1_b?-e?*OPO*/3O/3=O_c?-f?*OPO*/3O/3=O",
				"check sorting1");
		checkWURCS("WURCS=2.0/2,6,5/[o222h][a222h-1x_1-4]/1-2-2-2-2-2/c1-d1_e1-f1_a?-b1_b?-f?*OPO*/3O/3=O_c?-e?*OPO*/3O/3=O",
				"check sorting2");

		checkWURCS("WURCS=2.0/3,7,6/[axxxxh-1x_1-5_?*][axxxxh-1x_1-5_2*NCC/3=O][axxxxh-1x_1-5]/1-2-2-2-3-3-3/a?|b?|c?|d?|e?|f?|g?}-{a?|b?|c?|d?|e?|f?|g?_a?|b?|c?|d?|e?|f?|g?}-{a?|b?|c?|d?|e?|f?|g?_a?|b?|c?|d?|e?|f?|g?}-{a?|b?|c?|d?|e?|f?|g?_a?|b?|c?|d?|e?|f?|g?}-{a?|b?|c?|d?|e?|f?|g?_a?|b?|c?|d?|e?|f?|g?}-{a?|b?|c?|d?|e?|f?|g?_a?|b?|c?|d?|e?|f?|g?}-{a?|b?|c?|d?|e?|f?|g?",
				"correct composition with unknown deoxy position");
		checkWURCS("WURCS=2.0/3,8,7/[u2122h_2*NCC/3=O][u1122h][u2112h_2*NCC/3=O_?*OSO/3=O/3=O]/1-1-1-2-2-2-2-3/a?|b?|c?|d?|e?|f?|g?|h?}-{a?|b?|c?|d?|e?|f?|g?|h?_a?|b?|c?|d?|e?|f?|g?|h?}-{a?|b?|c?|d?|e?|f?|g?|h?_a?|b?|c?|d?|e?|f?|g?|h?}-{a?|b?|c?|d?|e?|f?|g?|h?_a?|b?|c?|d?|e?|f?|g?|h?}-{a?|b?|c?|d?|e?|f?|g?|h?_a?|b?|c?|d?|e?|f?|g?|h?}-{a?|b?|c?|d?|e?|f?|g?|h?_a?|b?|c?|d?|e?|f?|g?|h?}-{a?|b?|c?|d?|e?|f?|g?|h?_a?|b?|c?|d?|e?|f?|g?|h?}-{a?|b?|c?|d?|e?|f?|g?|h?",
				"correct composition with unknown position for sulfate");
		checkWURCS("WURCS=2.0/3,9,8/[u2122h_2*NCC/3=O][u1122h][u2112h_2*NCC/3=O_3|4*OSO/3=O/3=O]/1-1-1-1-2-2-2-3-3/a?|b?|c?|d?|e?|f?|g?|h?|i?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?_a?|b?|c?|d?|e?|f?|g?|h?|i?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?_a?|b?|c?|d?|e?|f?|g?|h?|i?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?_a?|b?|c?|d?|e?|f?|g?|h?|i?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?_a?|b?|c?|d?|e?|f?|g?|h?|i?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?_a?|b?|c?|d?|e?|f?|g?|h?|i?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?_a?|b?|c?|d?|e?|f?|g?|h?|i?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?_a?|b?|c?|d?|e?|f?|g?|h?|i?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?",
				"correct composition with alternative linkage position");

		checkWURCS("WURCS=2.0/3,8,7/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3-3-3-3/a4-b1_b4-c1_c6-d1_d3-e1_d3-g1_d6-h1_e2-f1",
				"wrong connection (d3-e1, d3-g1)");
		checkWURCS("WURCS=2.0/1,3,3/[a2122h-1b_1-5]/1-1-1/a4n3-b2n1-c6n2*1O(C^EC^ZC^EC^EC^EC^ZC^ZC^ZC^EC^EC^E$8/9^ZC^ZC^EC^E$3/14^E$5/7^ZC^EC^EC^EC^Z$6)/20O*2/18O/15O*3/13O/11O/10O_a3-b1_b3-c1",
				"check complex MAP");

		checkWURCS("WURCS=2.0/5,7,6/[<Q>_4*OP^XOP^XOCCN/5O/5=O/3O/3=O][a11221h-1x_1-5][a11221h-1x_1-5_6*OP^XOCCN/3O/3=O][a2122h-1a_1-5][a2122h-1b_1-5]/1-2-3-2-4-5-5/a5-b1_b3-c1_b4-f1_c2-d1_c3-e1_f4-g1",
				"Error?");

		checkWURCS("WURCS=2.0/1,4,3/[a2122h-1b_1-5]/1-1-1-1/b1-a3|a6_c1-a3|a6|b3|b6}_d1-a3|a6|b3|b6|c3|c6}", "???");
		checkWURCS("WURCS=2.0/2,19,19/[<Q>-?b][<Q>-?b_6*OCC/3=O]/1-1-1-1-2-1-2-2-2-2-1-1-1-1-2-2-2-2-2/a7-b1_b7-c1_c7-d1_d7-e1_e7-f1_f7-g1_g7-h1_h7-i1_i7-j1_j7-k1_k7-l1_l7-m1_m7-n1_n7-o1_o7-p1_p7-q1_q7-r1_r7-s1_a1-s7~n",
				"duplication1");
		checkWURCS("WURCS=2.0/2,19,19/[<Q>-?b_6*OCC/3=O][<Q>-?b]/1-1-1-1-1-2-2-2-2-1-1-1-1-2-1-2-2-2-2/a1-b7_b1-c7_c1-d7_d1-e7_e1-f7_f1-g7_g1-h7_h1-i7_i1-j7_j1-k7_k1-l7_l1-m7_m1-n7_n1-o7_o1-p7_p1-q7_q1-r7_r1-s7_a7-s1~n",
				"duplication2");
		checkWURCS("WURCS=2.0/5,26,25/[AUdxxxxxh_5*NCC/3=O][AUdxxxxxh_5*NCCO/3=O][uxxxxm][uxxxxh_2*NCC/3=O][uxxxxh]/1-1-2-2-2-3-3-3-3-3-4-4-4-4-4-4-4-4-5-5-5-5-5-5-5-5/a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?",
				"G45033KY-1");
		checkWURCS("WURCS=2.0/5,26,25/[AUdxxxxxh_5*NCCO/3=O][AUdxxxxxh_5*NCC/3=O][uxxxxm][uxxxxh_2*NCC/3=O][uxxxxh]/1-2-2-1-1-3-3-3-3-3-4-4-4-4-4-4-4-4-5-5-5-5-5-5-5-5/a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?|m?|n?|o?|p?|q?|r?|s?|t?|u?|v?|w?|x?|y?|z?",
				"G45033KY-2");
		checkWURCS("WURCS=2.0/2,2,2/[hxh][a2122h-1a_1-5_2*NCC/3=O]/1-2/a2-b1_a1-a3*OPO*/3O/3=O~n",
				"G71819IV-error?");
		checkWURCS("WURCS=2.0/2,2,3/[u1d21m][u2112h]/1-2/a?|b?}*OPO*/3O/3=O_a?|b?}*OPO*/3O/3=O_a?|b?}-{a?|b?",
				"generated by subsumption in error");
		checkWURCS("WURCS=2.0/1,6,7/[a2122h-1a_1-5]/1-1-1-1-1-1/a1-f4_a4-b1_b4-c1_c4-d1_d4-e1_e3-f2*OO*_e4-f1",
				"error cyclodex1");
		checkWURCS("WURCS=2.0/1,6,7/[a2122h-1a_1-5]/1-1-1-1-1-1/a1-f4_a4-b1_b4-c1_c4-d1_d3-e2*OO*_d4-e1_e4-f1",
				"error cyclodex2");
		checkWURCS("WURCS=2.0/1,6,7/[a2122h-1a_1-5]/1-1-1-1-1-1/a1-f4_a4-b1_b4-c1_c3-d2*OO*_c4-d1_d4-e1_e4-f1",
				"error cyclodex3");
		checkWURCS("WURCS=2.0/1,6,7/[a2122h-1a_1-5]/1-1-1-1-1-1/a1-f4_a4-b1_b3-c2*OO*_b4-c1_c4-d1_d4-e1_e4-f1",
				"error cyclodex4");
		checkWURCS("WURCS=2.0/1,6,7/[a2122h-1a_1-5]/1-1-1-1-1-1/a1-f4_a3-b2*OO*_a4-b1_b4-c1_c4-d1_d4-e1_e4-f1",
				"error cyclodex5");
		checkWURCS("WURCS=2.0/5,17,16/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a2112h-1x_1-5][Aad21122h-2x_2-6_5*NCCO/3=O][a1221m-1x_1-5]/1-1-2-2-1-3-4-4-5-2-1-1-5-1-3-4-5/a?-b1_a?-m1_b?-c1_c?-d1_c?-j1_c?-l1_d?-e1_e?-f1_e?-i1_f?-g2_g?-h2_j?-k1_n?-o1_n?-q1_o?-p2_n1-d?|j?}",
				"test normalization");
		checkWURCS("WURCS=2.0/2,6,5/[a222h-1b_1-4_1*N_3*OPO/3O/3=O][a222h-1b_1-4_1*N]/1-2-2-2-2-2/a5-b3*OPO*/3O/3=O_b5-c3*OPO*/3O/3=O_c5-d3*OPO*/3O/3=O_d5-e3*OPO*/3O/3=O_e5-f3*OPO*/3O/3=O",
				"strange residue orders in DNA (but normalization is OK...)");
		checkWURCS("WURCS=2.0/1,1,0/[a2122h-1x_1-5**_2*NCC/3=O]/1/",
				"error direct connection between backbone carbon");

		checkWURCS("WURCS=2.0/1,2,1/[ha122h-2b_2-4]/1-1/a2-a3-b2-b3*OBO*/3O*/4O*",
				"error in CollectSequence");

		checkWURCS("WURCS=2.0/1,1,0/[Aad21121h-2x_2-6_%?%4*O@_%?%5*N@_%?%7*O@_%?%8*O@_%?%9*O@]/1/",
				"correct sialic acid");
		checkWURCS("WURCS=2.0/1,1,0/[Aad21121h-2x_2-6_%?%4*OX_%?%5*NR_%?%7*OA_%?%8*OQ_%?%9*ONp]/1/",
				"correct unknown atom in MAP");

		checkWURCS("WURCS=2.0/1,1,0/[hxxxxh_1-4]/1/", "normalization error 1-1");
		checkWURCS("WURCS=2.0/1,1,0/[hxxxxh_3-6]/1/", "normalization error 1-2");
		checkWURCS("WURCS=2.0/1,1,0/[hxxxxh_1-5]/1/", "normalization error 2-1");
		checkWURCS("WURCS=2.0/1,1,0/[hxxxxh_2-6]/1/", "normalization error 2-2");
//		LinkedList<String> t_aWURCSList = new LinkedList<String>();
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[u2122h]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[u2g22h]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[u2L22h]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[u2122h_1-5]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[u2122h-1x]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[u2122h-1x_1-5]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[hU2122h-1x_2-6]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[hU2122h_2-6]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[a622h-1b_1-5]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[a622h-1b_1-5_2*CO]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[o2122h]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[o2122h]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[oddddh]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[o2<x>22h]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[o2<xx>22h]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[a2122h]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[a2122h_1-5]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[a2122h-1a_1-5]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[a2122h-?a_1-5]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[h2122h-1a_1-5]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[h2122h_1-5]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[h2122h-1a]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[h2122h]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[a2122h-1a_1-5]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[a2122h-1g_1-5]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,3,4/[a2122h-1a_1-5]/1-1-1/a4-b1_b4-c1_c3-b4_b4-a3");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[a2OO2h-1a_2-5]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,2,1/[a2122h-1a_1-5]/1-1/a1-a2-b3");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[u2UO2h-1a_2-5]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[a2122a-1a]/1/");
//		t_aWURCSList.add("WURCS=2.0/3,31,30/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a1122h-1x_1-5_?*OPO/3O/3=O]/1-1-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-3-2-2/a?-b1_b?-c1_c?-d1_c?-B1_d?-e1_d?-z1_e?-f1_e?-w1_f?-g1_f?-r1_g?-h1_g?-o1_h?-i1_h?-l1_i?-j1_j?-k1_l?-m1_l?-n1_o?-p1_p?-q1_r?-s1*OPO*/3O/3=O_r?-u1_s?-t1_u?-v1_w?-x1_x?-y1_z?-A1_B?-C1_B?-E1_C?-D1\n");
//		t_aWURCSList.add("WURCS=2.0/3,3,2/[a2122A-1x_1-5][a211h-1x_1-4][a211h-1x_1-?]/1-2-3/a?-b1_a?-c1");
//		t_aWURCSList.add("WURCS=2.0/4,4,4/[<Q>-?a_7-9*OC^XO*/3CO/6=O/3C_5*OCC/3=O][a2122h-1a_1-5][a2122h-1b_1-5][a1122h-1b_1-5]/1-2-3-4/a4-b1_b4-c1_c4-d1_a2-d3~n");
//		t_aWURCSList.add("WURCS=2.0/2,4,3/[a122h-1x_1-4][<Q>]/1-1-2-2/a5-b1_b3-c1_b5-d1");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[h2122222222222222h]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[a2122ah-1a_1-5]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[hU122h]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[hO122h_2*=N]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[ha122h-2x_2-6]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,2,0/[ha122h-2x_2-6]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[ha122h-1x_2-6]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[ha522h-2x_2-6]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[A1EE1h]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[A1EE1h-1x]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[A1EE1h_2-4]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[A1EZ1h]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,2,1/[a2122h-1b_1-5]/1-1/a?-b3*OSO*/3=O/3=O");
//		t_aWURCSList.add("WURCS=2.0/3,4,4/[hxh][a2122h-1x_1-5][a2122h-1a_1-5]/1-2-3-3/a3n2-b1n1*1NCCOP^XO*2/6O6=O_b4-c1_c4-d1_c1-c4~n");
//		t_aWURCSList.add("WURCS=2.0/3,4,4/[hxh][a2122h-1x_1-5][a2122h-1a_1-5]/1-2-3-3/a3n2-b1n1*1NCCOP^XO*3/6O6=O_b4-c1_c4-d1_c1-c4~n");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[Ad2d112h_3-7_5n2-6n1*1O(CCC^ECC^ZC$3)/6NC(CC^ZCC^ZNC$11)/4*2/10=O_1*NC(C^ZCC^ZCC^EC$4)/8F/5F]/1/");
//		t_aWURCSList.add("WURCS=2.0/4,12,10+/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4-4-4-4-4-4-4-4/a4-b1_b4-c1_c3-d1_c6-h1_d2-e1_d6-g1_e2-f1_h3-i1_h6-j1_j2-k1");
//		t_aWURCSList.add("WURCS=2.0/4,12,10/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4-4-4-4-4-4-4-4/a4-b1_b4-c1_c3-d1_c6-h1_d2-e1_d6-g1_e2-f1_h3-i1_h6-j1_j2-k1");
//		t_aWURCSList.add("WURCS=2.0/4,12,11/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4-4-4-4-4-4-4-4/a4-b1_b4-c1_c3-d1_c6-h1_d2-e1_d6-g1_e2-f1_h3-i1_h6-j1_j2-k1");
//		t_aWURCSList.add("WURCS=2.0/5,15,8/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a2112h-1x_1-5][Aad21122h-2x_2-6_5*NCC/3=O][Aad21122h-2x_2-6_5*NCCO/3=O]/1-1-2-2-1-3-2-1-3-1-3-4-4-4-5/a?-b1_b?-c1_c?-d1_c?-g1_d?-e1_e?-f1_g?-h1_h?-i1");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[a2nh-1a_1-4_2*CO]/1/");
//		t_aWURCSList.add("WURCS=2.0/2,13,12/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-2-2-2-2-2-2-2-2-2-2-2-2/a?-b1_b?-c1_b?-i1_c?-d1_c?-g1_d?-e1_d?-f1*OPO*/3O/3=O_g?-h1_i?-j1_i?-l1_j?-k1_l?-m1");
//		t_aWURCSList.add("WURCS=2.0/2,13,12/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-2-2-2-2-2-2-2-2-2-2-2-2/a?-b1_b?-c1_b?-i1_c?-d1_c?-g1_d?-e1_d?-f1*OPO*/3O*/3=O_g?-h1_i?-j1_i?-l1_j?-k1_l?-m1");
//		t_aWURCSList.add("WURCS=2.0/2,13,12/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-2-2-2-2-2-2-2-2-2-2-2-2/a?-b4_b4-c1_b4-i1_c?-d1_c?-g1_d?-e1_d?-f1*OPO*/3O/3=O_g?-h1_i?-j1_i?-l1_j?-k1_l?-m1");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[a2122h-1x_1-5_2*NCC/3=O]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[a2122h-1x_1-5_2*]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[axxxxh-1x_1n2-2n1*1OC(CCCC^ZCC$4)/8O/7OC/6O/5*2/3=O_1-5]/1/");
//		t_aWURCSList.add("WURCS=2.0/1,1,0/[axxxxh-1x_1n2-2n1*1OC(CCCC^ZCC$4)/8O/7OC/6O/5*/3=O_1-5]/1/");
//		t_aWURCSList.add("WURCS=2.0/2,6,5/[o222h][a222h-1x_1-4]/1-2-2-2-2-2/c1-d1_e1-f1_a?-b1_b?-f?*OPO*/3O/3=O_c?-e?*OPO*/3O/3=O");
//		t_aWURCSList.add("WURCS=2.0/2,6,5/[o222h][a222h-1x_1-4]/1-2-2-2-2-2/c1-d1_e1-f1_a?-b1_b?-e?*OPO*/3O/3=O_c?-f?*OPO*/3O/3=O");
//		t_aWURCSList.add("WURCS=2.0/1,2,1/[11m_1*OCC/3=O]/1-1/a1n3-a2n1-b1n4-b2n2*1OC(CCCC^ZCCCC^ZCCC^ECC^ECC$9/18$13/8C^ECC^ZCC$4/22$7)/23O/21OC/19OC/16OC/14OC/12O/11CO*2/10*4/5*3");
//		t_aWURCSList.add("WURCS=2.0/3,3,1/[A11dCd12dd1d1d1dA][a1122m-1b_1-5_3*N][ad111m-1x_1-5]/1-2-3/a2n4-a5d1-a17n3-b1n2-c1n5*1OC^R*4/3CC^RO*2/6C=^ECC=^ECC=^ECC=^ECCCC=^ECC=^ECC^SC^RO*5/24C^SC^SO*3/28C/27C/23C");
//		t_aWURCSList.add("WURCS=2.0/3,3,1/[A11dCd12dd1d1d1dA][a1122m-1b_1-5_3*N][ad111m-1x_1-5]/1-2-3/a2n4-a5d1-a17n3-b1n2-c1n5*1OC^R*4/3CC^RO*2/6C=^ECC=^ECC=^ECC=^ECCCC=^ECC=^ECC^SC^RO*6/24C^SC^SO*3/28C/27C/23C");
//
//		for ( String t_strWURCS : t_aWURCSList ) {
//			System.out.println(t_strWURCS+": ");
//			WURCSValidator validator=new WURCSValidator();
//			validator.start(t_strWURCS);
//			System.out.println( validator.getReport().getReport() );
//			if(validator.getErrors().size()==0)
//				System.out.println(validator.getStandardWURCS());
//			System.out.println("the number of errors: "+validator.getErrors().size());
//			for(String er: validator.getErrors())
//				System.out.println("	Error:   "+er);
//			for(String wa: validator.getWarnings())
//				System.out.println("	Warning: "+wa);
//			System.out.println();
//		}
	}

	private static void checkWURCS(String a_strWURCS, String a_strComment) {
		System.out.println(a_strWURCS+": ");
		WURCSValidator validator=new WURCSValidator();
		validator.start(a_strWURCS);
		if ( a_strComment != null )
			System.out.println("Comment: "+a_strComment);
		System.out.println(validator.getReport().getResults());
//		System.out.println(validator.getReport().getResultsSimple());
		System.out.println("Normalized: "+validator.getReport().getStandardString());
		System.out.println();
	}
}
