package org.glycoinfo.WURCSFramework.test;

import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.util.graph.WURCSGraphSeparatorWithAglycone;
import org.glycoinfo.WURCSFramework.util.graph.WURCSGraphSeparatorWithAglycone.WURCSGraphForAglycone;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;

public class TestWURCSGraphSeparator {

	public static void main(String[] args) {
		List<String> lWURCSs = new ArrayList<>();
		lWURCSs.add("WURCS=2.0/1,1,0/[a2122h-1a_1-5_1*OC]/1/");
		lWURCSs.add("WURCS=2.0/1,1,0/[a2122h-1a_1-5_1*N]/1/");
		lWURCSs.add("WURCS=2.0/1,2,1/[a2122h-1a_1-5]/1-1/a1-b1");
		lWURCSs.add("WURCS=2.0/1,2,1/[a2122h-1a_1-5]/1-1/a1-b1*N*");
		lWURCSs.add("WURCS=2.0/1,2,1/[a2122h-1a_1-5]/1-1/a1-b1*OCO*");

		for ( String strWURCS : lWURCSs ) {
			try {
				separate(strWURCS);
			} catch (WURCSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void separate(String a_strWURCS) throws WURCSException {
		WURCSFactory factory = new WURCSFactory(a_strWURCS);

		WURCSGraphForAglycone ret = WURCSGraphSeparatorWithAglycone.start(factory.getGraph());

		System.out.println("INPUT:  "+a_strWURCS);
		if ( !ret.isSeparated() ) {
			System.out.println("No separation");
			System.out.println();
			return;
		}

		for ( WURCSGraph sep : ret.getSeparatedGraphs() ) {
			WURCSFactory factorySep = new WURCSFactory(sep);
			System.out.println("OUTPUT: "+factorySep.getWURCS());
		}
		System.out.println();
	}

	public static List<String> separateWithAglycone(String a_strWURCS) throws WURCSException {
		WURCSFactory factory = new WURCSFactory(a_strWURCS);

		WURCSGraphForAglycone ret = WURCSGraphSeparatorWithAglycone.start(factory.getGraph());

		List<String> lSeparatedWURCSs = new ArrayList<>();

		if ( !ret.hasAglycone() ) {
			// Return input string if no separation
			lSeparatedWURCSs.add(a_strWURCS);
			return lSeparatedWURCSs;
		}

		for ( WURCSGraph sep : ret.getSeparatedGraphs() ) {
			WURCSFactory factorySep = new WURCSFactory(sep);
			lSeparatedWURCSs.add(factorySep.getWURCS());
		}

		return lSeparatedWURCSs;
	}

}
