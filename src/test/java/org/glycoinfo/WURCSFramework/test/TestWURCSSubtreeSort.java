package org.glycoinfo.WURCSFramework.test;

import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;

public class TestWURCSSubtreeSort {

	public static void main(String[] args) {

		List<String> t_lWURCSBefores = new ArrayList<>();
		// Correct residue order
		t_lWURCSBefores.add("WURCS=2.0/3,6,5/[a2112h-1x_1-5_2*NCC/3=O][a2112h-1x_1-5][a2122h-1x_1-5_2*NCC/3=O]/1-2-3-2-3-2/a?-b1_c?-d1_e?-f1_c1-a?|b?}_e1-a?|b?}");
		t_lWURCSBefores.add("WURCS=2.0/6,15,14/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a2112m-1a_1-5][Aad21122h-2a_2-6_5*NCC/3=O]/1-1-2-3-1-4-1-4-3-1-4-5-6-6-6/a4-b1_a6-l1_b4-c1_c3-d1_c6-i1_d2-e1_d4-g1_e4-f1_g4-h1_i2-j1_j4-k1_m2-f3|h3|k3}_n2-f3|h3|k3}_o2-f6|h6|k6}");
		// Wrong residue order
		t_lWURCSBefores.add("WURCS=2.0/3,6,5/[a2122h-1x_1-5_2*NCC/3=O][a2112h-1x_1-5][a2112h-1x_1-5_2*NCC/3=O]/1-2-1-2-3-2/a?-b1_c?-d1_e?-f1_a1-e?|f?}_c1-e?|f?}");
		t_lWURCSBefores.add("WURCS=2.0/3,6,5/[a2112h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a2112h-1b_1-5]/1-2-3-2-3-3/a3-f1_b4-c1_d4-e1_b1-a?|f?}_d1-a?|f?}");
		t_lWURCSBefores.add("WURCS=2.0/6,15,14/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][Aad21122h-2a_2-6_5*NCC/3=O][a2112m-1a_1-5]/1-1-2-3-1-4-5-5-5-1-4-3-1-4-6/a4-b1_a6-o1_b4-c1_c3-d1_c6-l1_d2-e1_d4-j1_e4-f1_j4-k1_l2-m1_m4-n1_g2-f3|k3|n3}_h2-f3|k3|n3}_i2-f6|k6|n6}");
		t_lWURCSBefores.add("WURCS=2.0/6,12,11/[a2122h-1b_1-5_2*NCC/3=O][a1221m-1a_1-5][a2112h-1b_1-5][Aad21122h-2a_2-6_5*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-4-1-3-1-5-6-6-2/a4-h1_a6-l1_b3-c1_b4-d1_f4-g1_h4-i1_i3-j1_i6-k1_b1-a2|h2|i2|j2|k2|l2}_f1-a2|h2|i2|j2|k2|l2}_d?-e2");
		// Wrong residue order with two hierarchical subtrees
		t_lWURCSBefores.add("WURCS=2.0/3,10,9/[a2122h-1x_1-5_2*NCC/3=O][a2112h-1x_1-5][a2112h-1x_1-5_2*NCC/3=O]/1-2-1-2-1-2-3-2-3-2/a?-b1_c?-d1_e?-f1_g?-h1_i?-j1_a1-e?|f?}_c1-e?|f?}_e1-i?|j?}_g1-i?|j?}");
		t_lWURCSBefores.add("WURCS=2.0/3,14,13/[a2122h-1x_1-5_2*NCC/3=O][a2112h-1x_1-5][a2112h-1x_1-5_2*NCC/3=O]/1-2-1-2-1-2-3-2-3-2-3-2-3-2/a?-b1_c?-d1_e?-f1_g?-h1_i?-j1_k?-l1_m?-n1_a1-i?|j?}_c1-i?|j?}_e1-k?|l?}_g1-k?|l?}_i1-m?|n?}_k1-m?|n?}");
		// Wrong residue order with complex hierarchical subtrees
		t_lWURCSBefores.add("WURCS=2.0/3,14,13/[a2122h-1x_1-5_2*NCC/3=O][a2112h-1x_1-5][a2112h-1x_1-5_2*NCC/3=O]/1-2-1-2-1-2-3-2-3-2-3-2-3-2/a?-b1_c?-d1_e?-f1_g?-h1_i?-j1_k?-l1_m?-n1_a1-i?|j?|m?|n?}_c1-i?|j?}_e1-k?|l?|m?|n?}_g1-k?|l?}_i1-m?|n?}_k1-m?|n?}");
		// Wrong residue order with two hierarchical subtrees and cyclic subtree relationship
		t_lWURCSBefores.add("WURCS=2.0/3,10,10/[a2122h-1x_1-5_2*NCC/3=O][a2112h-1x_1-5][a2112h-1x_1-5_2*NCC/3=O]/1-2-1-2-1-2-3-2-3-2/a?-b1_c?-d1_e?-f1_g?-h1_i?-j1_a1-e?|f?}_c1-e?|f?}_e1-i?|j?}_g1-i?|j?}_i1-a?|b?}");

		for ( String t_strWURCSBefore : t_lWURCSBefores ) {
			System.out.println(t_strWURCSBefore);
			try {
				WURCSFactory t_factory = new WURCSFactory(t_strWURCSBefore);
				String t_strWURCSAfter = t_factory.getWURCS();
				if ( !t_strWURCSBefore.equals(t_strWURCSAfter) )
					System.out.println(t_strWURCSAfter);
				else
					System.out.println("Unsorted");
				System.out.println();
			} catch (WURCSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
