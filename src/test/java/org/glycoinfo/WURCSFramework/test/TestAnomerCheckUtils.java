package org.glycoinfo.WURCSFramework.test;

import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.WURCSFramework.util.AnomerCheckUtils;

public class TestAnomerCheckUtils {

	public static void main(String[] args) {

		// For WURCSs
		List<String> lWURCSs = new ArrayList<>();
		// Glc(a1) - monosaccharide with alfa anomer
		lWURCSs.add("WURCS=2.0/1,1,0/[a2122h-1a_1-5]/1/");
		// Man(a1-6)[Man(a1-3)]Man(b1-4)GlcNAc(b1-4)GlcNAc(b1) - beta anomer on reducing end
		lWURCSs.add("WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1");
		// Man(a1-6)[Man(a1-3)]Man(b1-4)GlcNAc(b1-4)GlcNAc(x1) - with anomer with unknown configuration on reducing end
		lWURCSs.add("WURCS=2.0/4,5,4/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4/a4-b1_b4-c1_c3-d1_c6-e1");
		// Man(a1-6)[Man(a1-3)]Man(b1-4)GlcNAc(b1-4)aldehyde-GlcNAc - open chain on reducing end
		lWURCSs.add("WURCS=2.0/4,5,4/[o2122h_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4/a4-b1_b4-c1_c3-d1_c6-e1");
		// Man(a1-6)[Man(a1-3)]Man(b1-4)GlcNAc(b1-4)GlcNAc-ol - alditol on reducing end
		lWURCSs.add("WURCS=2.0/4,5,4/[h2122h_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4/a4-b1_b4-c1_c3-d1_c6-e1");
		// Man(a1-6)[Man(a1-3)]Man(b1-4)GlcNAc(b1-4)GlcNAc(?) - uncertain anomeric state on reducing end
		lWURCSs.add("WURCS=2.0/4,5,4/[u2122h_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4/a4-b1_b4-c1_c3-d1_c6-e1");
		// Glc(a1-a1)Glc - trehalose (non-reducing sugar)
		lWURCSs.add("WURCS=2.0/1,2,1/[a2122h-1a_1-5]/1-1/a1-b1");
		// Man(a1-6)[Man(a1-3)]Man(b1-4)GlcNAc(b1-4)GlcNAc(b1-)N - beta anomer on reducing end but connecting to N
		lWURCSs.add("WURCS=2.0/4,5,4/[a2122h-1b_1-5_1*N_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-4/a4-b1_b4-c1_c3-d1_c6-e1");
		// GlcNAc(x1)x2,Man(x1)x3 - composition
		lWURCSs.add("WURCS=2.0/2,5,4/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-1-2-2-2/a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?");

		for ( String strWURCS : lWURCSs )
			checkAnomer(strWURCS);

		// For WURCSs for composition
		List<String> lWURCSCompositions = new ArrayList<>();
		// GlcNAc(x1) x2,Man(x1) x3 - composition which all residues has anomer with unknown configuration
		lWURCSCompositions.add("WURCS=2.0/2,5,4/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-1-2-2-2/a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?");
		// GlcNAc(?) x2,Man(?) x3 - composition which all residues has uncertain anomeric state
		lWURCSCompositions.add("WURCS=2.0/2,5,4/[u2122h_2*NCC/3=O][u1122h]/1-1-2-2-2/a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?");
		// GlcNAc(b1),GlcNAc(?),Man(?) x3 - composition which only one residue has uncertain anomeric state
		lWURCSCompositions.add("WURCS=2.0/3,5,4/[u2122h_2*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-2-3-3-3/a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?");
		// Rib-ol x5 - composition which all residues has uncertain anomeric state
		lWURCSCompositions.add("WURCS=2.0/1,5,4/[h222h]/1-1-1-1-1/a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?");

		for ( String strWURCSCompo : lWURCSCompositions )
			checkValidCompositionForGlycosylation(strWURCSCompo);

		// For ResidueCodes
		List<String> lResidueCodes = new ArrayList<>();
		lResidueCodes.add("a2122h-1a_1-5");
		lResidueCodes.add("a2122h-1b_1-5");
		lResidueCodes.add("a2122h-1x_1-5");
		lResidueCodes.add("o2122h");
		lResidueCodes.add("u2122h");
		lResidueCodes.add("h2122h");
		lResidueCodes.add("<Q>");
		lResidueCodes.add("<Q>-?a");
		lResidueCodes.add("<Q>-?b");
		lResidueCodes.add("<Q>-?x");
		lResidueCodes.add("<Q>-0o");

		for ( String strResCode : lResidueCodes )
			checkAnomerForResidueCode(strResCode);
	}

	private static void checkAnomer(String a_strWURCS) {
		System.out.println("Input WURCS:");
		System.out.println(a_strWURCS);

		String strAnomSymbol = AnomerCheckUtils.getAnomericSymbol(a_strWURCS);

		System.out.println("Anomer on reducing end: "+strAnomSymbol);
		System.out.println();
	}

	private static void checkValidCompositionForGlycosylation(String a_strWURCSCompo) {
		System.out.println("Input WURCS for composition:");
		System.out.println(a_strWURCSCompo);

		boolean isValid = AnomerCheckUtils.isValidCompositionForGlycosylation(a_strWURCSCompo);

		System.out.println("Is available for glycosylation: "+((isValid)? "yes" : "no"));
		System.out.println();
	}

	private static void checkAnomerForResidueCode(String a_strResCode) {
		System.out.println("Input ResidueCode: "+a_strResCode);

		String strAnomSymbol = AnomerCheckUtils.getAnomericSymbolForResidueCode(a_strResCode);

		System.out.println("Anomer on reducing end: "+strAnomSymbol);
		System.out.println();
	}
}
