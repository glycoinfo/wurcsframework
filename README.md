# WURCSFramework

WURCSFramework is a software library for providing functionalities for WURCS such as reading/writing WURCS string format

"WURCS" stands for <u>W</u>eb3.0 <u>U</u>nique <u>R</u>epresentation of <u>C</u>arbohydrate <u>S</u>tructure  
* Matsubara, M. *et al*., *J. Chem. Inf. Model*., 57, 632-37 (2017)

Contact: yamadaissaku@gmail.com

## Features

* Read and write WURCS string format
* Convert WURCS string to a data structure to handle carbohydrate structure as graph (WURCSGraph)
* Sort monosaccharide residues and linkages based on a normalization rule to make the WURCS string unique
* Calculate mass of the carbohydrate structure based on the structural information described in the WURCS

## Requirement

* Software development language: Java 1.8 or more
* Build tool: maven 3.5 or higher

## Build

```
$ mvn clean install
```

## Install

Copy and paste this inside your `pom.xml` `dependencies` block.

```
<dependency>
  <groupId>org.glycoinfo</groupId>
  <artifactId>wurcsframework</artifactId>
  <version>1.2.12</version>
</dependency>
```

If you do not do registry setup yet, you need to add following into your `pom.xml`.
```
<repositories>
  <repository>
    <id>WURCSFramework-gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/17725126/packages/maven</url>
  </repository>
</repositories>
```

## Examples

### To validate WURCS

```java
import org.glycoinfo.WURCSFramework.util.validation.WURCSValidator;

public class WURCSValidatorTest {
	public static void main(String[] args) {

		String strWURCS = "WURCS=2.0/1,1,0/[a2122h-1b_1-5_2*NCC/3=O]/1/";

		WURCSValidator validator = new WURCSValidator();

		// Set limit of branch count on a MAP (default=10)
		validator.setMaxBranchCount(15);

		// Start validation
		validator.start(strWURCS);

		// Check if the WURCS has error
		if ( validator.getReport().hasError() )
			System.out.println("The input has error.");

		// Check if the WURCS can not be validated
		if ( validator.getReport().hasUnverifiable() )
			System.out.println("The input can not be validated.");

		// Check if the WURCS changed in normalization process
		if ( validator.getReport().isStandardized() )
			System.out.println("The input changed in normalization process.") 

		// Output validation results
		System.out.println(validator.getReport().getResults());

		// Output normalized WURCS
		System.out.println(validator.getReport().getStandardString());
	}
}
```

## Author

#### Masaaki Matsubara
* The Noguchi Institute, Japan

#### Issaku Yamada
* The Noguchi Institute, Japan

## Release note
* 2024/05/09: released ver 1.3.1
    * Added MAP normalization to remove stereochemistry on aromatic ring
    * Updated validation report to see standardization result
* 2024/04/30: released ver 1.3.0
    * Added MAP normalization (only representation for branch on aromatic)
    * Changed validation error to warning for aromatic ring with stereochemistry
* 2024/03/28: released ver 1.2.16
    * Added comparison between ring modifications on a monosaccharide
* 2024/01/23: released ver 1.2.15
    * Updated comparison for leaf modifications
    * Added validation error for aromatic ring with stereochemistry
* 2023/08/02: released ver 1.2.14
    * Fixed comparison between backbones connecting with the same modification
* 2023/03/27: released ver 1.2.13
    * Added validation result type "unverifiable"
* 2023/03/16: modified build CI for API
* 2023/01/20: released ver 1.2.12
    * Fixed wrong ordering for StarIndices on MAP
* 2022/12/15: released ver 1.2.11
    * Handled "out of bounds" error of RESIndex
    * Handled exceptions other than WURCSException at WURCSValidator
* 2022/12/13: released ver 1.2.10
    * Fixed ordering MAP connections
* 2022/07/07: released ver 1.2.9
    * Fixed MAP representation for double bond on branch point in aromatic ring
* 2022/06/29: released ver 1.2.8
    * Fixed validation error for aromatic ring without ring descriptor
    * Fixed MAP representation for branch point in aromatic ring
* 2022/06/23: released ver 1.2.7
    * Added validation error for aromatic ring without ring descriptor
    * Updated normalization process to be multi-step
    * Removed validation warning for aromatic ring without stereochemistry
* 2022/03/11: released ver 1.2.6
    * Fixed atom stereo calculation
* 2021/10/18: released ver 1.2.5
    * Fixed normalization error for symmetric monosaccharides with ring substitution
    * Added MAP validation for unknown atoms
* 2021/08/23: released ver 1.2.4
    * Changed symbol for atom groups in MAP from "R" to "@"
    * Added MAP format checks
* 2021/05/17: released ver 1.2.3
    * Fixed normalization error for modifications with combination of anomeric and non-anomeric linkages
* 2021/04/23: released ver 1.2.2
    * Added new method for separating WURCSGraph with aglycones
* 2021/03/24: released ver 1.2.1
    * Fixed Backbone.getAnomericEdge()
* 2021/03/19: released ver 1.2.0
    * Added systems for getting anomeric state on reducing end
* 2021/03/08: released ver 1.1.0
    * Updated MAPGraph normalization system
    * Made WURCSValidator extendable
    * Updated CarbonDescriptor classes not to use hard-coded parameters
    * Fixed method and class names
    * Attached sources into .jar file
* 2020/07/30: released ver 1.0.1
    * Fixed inversion probrem for unknown backbones
* 2020/07/22: released ver 1.0.0
    * Updated validation system